﻿using UnityEngine;
using System.Collections;

/* Code from: iX Special 1/2015 "Spiele entwickeln"
 * Code by Prof. Dr. Christian Geiger and Patrick Pogscheba, FH Düsseldorf
 */

[RequireComponent(typeof(CharacterController))]  							
public class ThirdPersonController : MonoBehaviour {
	
	public float rotationSpeed = 60.0f;										
	public float walkingSpeed = 8.0f;										
	public float jumpSpeed = 8.0f;
	public float mouseSensitivity = 10;
	public float airSpeed 	= 5f;
	public float gravity = 9.81f;
	
	
	Vector3 velocity = Vector3.zero;
	
	CharacterController characterController;    							
	
	// Use this for initialization
	void Start () {
		characterController = this.GetComponent<CharacterController>();   
	}

    void OnGUI()
    {
        //if (!Debug.isDebugBuild)
        {
            if (GUI.Button(new Rect(0, 20, 100, 20), "Reset Player"))
            {
                //TODO: find suitable start position -> points of interest
                Vector3 pos = new Vector3();

                switch (Application.loadedLevelName)
                {
                    case "Landscape": 
                        pos = new Vector3(0, 300, 0); 
                        break;

                    case "Cave":
                        pos = new Vector3(100, 35, 100);
                        break;

                    case "Dungeon":
                        pos = new Vector3(15, 1.5f, 15);
                        break;

                    default: throw new UnityException("TPS.OnGUI: unknown scene loaded");
                    
                }

                gameObject.transform.position = pos;
                gameObject.transform.rotation = new Quaternion(0, 0, 0, 0);
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		float forward = Input.GetAxis("Vertical");					
		float strafe = Input.GetAxis("Strafe");						
		float rotate = Input.GetAxis("Horizontal") * rotationSpeed;			
		
		Vector3 airVelocity= Vector3.zero;
		
		// Drehung des Spielers per Maus
		if(Input.GetMouseButton(1))
		{
			rotate = Input.GetAxis("Mouse X") * mouseSensitivity;
		}
		
		if(characterController.isGrounded)
		{
			velocity = this.transform.right * walkingSpeed * strafe +  this.transform.forward * walkingSpeed * forward;				
			
            // deactivate Jump to prevent players from going out of bounds, also not needed for game concept.
            /*
			if(Input.GetButton ("Jump"))
			{
				velocity.y = jumpSpeed;
				
			}
            */
		}
		else // Anpassung der Bewegung in der Luft
		{
			airVelocity = forward * airSpeed * this.transform.forward + strafe * airSpeed * this.transform.right;
		}		
		
		// Gravitation 
		velocity.y-=gravity * Time.deltaTime;
		
		characterController.Move((velocity + airVelocity)  *  Time.deltaTime);			
		transform.Rotate(Vector3.up, rotate  * Time.deltaTime);
	}
}
