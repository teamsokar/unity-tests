﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnOverworldButtonClicked()
    {
        LoadingScreen.show();
        Application.LoadLevel("Landscape");
    }

    public void OnCaveButtonClicked()
    {
        LoadingScreen.show();
        Application.LoadLevel("Cave");
    }

    public void OnDungeonButtonClicked()
    {
        LoadingScreen.show();
        Application.LoadLevel("Dungeon");
    }

    public void OnOptionsButtonClicked(){
        Application.LoadLevel("OptionsMenu");
    }

    public void OnExitButtonClicked(){
        Application.Quit(); // does nothing in browser
    }
}
