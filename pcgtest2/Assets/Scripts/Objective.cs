﻿using UnityEngine;
using System.Collections;

public class Objective : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Function is called when another object collides with the current one
    /// 
    /// </summary>
    /// <param name="collision">collision</param>
    void OnCollisionEnter(Collision collision)
    {
        this.rigidbody.isKinematic = true;
        this.collider.isTrigger = true;
    }
}
