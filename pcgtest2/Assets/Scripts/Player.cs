﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    private GameManager gm;

	/// <summary>
	/// Initialization
	/// </summary>
	void Awake () {
        gm = FindObjectOfType<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Function called if player collides with another object
    /// Handles objectives
    /// </summary>
    /// <param name="other">collider component of other object</param>
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Player: Collission detected with: " + other.name);
        if (other.CompareTag("Objective"))
        {
            switch (other.name)
            {
                case "startPoint(Clone)":
                    {
                        // check current scene; if not landscape, go back
                        if (gm != null && Application.loadedLevelName != "Landscape")
                        {
                            //TODO display message to check if player wants to leave
                            //gm.SendMessage("InitiateLevelTransition", "Landscape");
                            gm.InitiateLevelTransition("Landscape", this.transform.position);
                        }
                        break;
                    }
                case "finishPoint(Clone)":
                    {
                        // check current objective if finished
                        if (gm != null && gm.Mg.CheckObjectivesFinished())
                        {
                            if (Application.loadedLevelName != "Landscape")
                            {
                                switch (Application.loadedLevelName)
                                {
                                    case "Dungeon":
                                        {
                                            gm.ObjectiveCompleted("Overworld Dungeon", true); // pass key of constants dictionary of Lsystem
                                            break;
                                        }
                                    case "Cave":
                                        {
                                            gm.ObjectiveCompleted("Overworld Cave", true); // pass key of constants dictionary of Lsystem
                                            break;
                                        }
                                }

                                gm.InitiateLevelTransition("Landscape", this.transform.position);
                            }
                        } else{
                            //TODO display message to check if player wants to leave anyway
                            Debug.Log("Player: not all objectives finished");
                            gm.Infotext = "not all objectives finished";
                        }
                        break;
                    }
                case "Enemy(Clone)":
                case "Find(Clone)":
                case "Key(Clone)":
                    {
                        // handle objective change
                        //Debug.Log("Player: handle objective change");
                        if (gm != null)
                        {
                            //gm.SendMessage("ObjectiveCompleted", other.name.Substring(0,other.name.Length-7));
                            //gm.SendMessage("ObjectiveCompleted", other.GetComponent<Objective>());
                            gm.ObjectiveCompleted(other.GetComponent<Objective>());
                            Destroy(other.gameObject);
                        }
                        break;
                    }

                case "Boss(Clone)":
                case "Chest(Clone)":
                    {
                        if (gm != null)
                        {
                            if (gm.PrerequierementObjectivesCompleted(other.GetComponent<Objective>()))
                            {
                                //gm.SendMessage("ObjectiveCompleted", other.GetComponent<Objective>());
                                gm.ObjectiveCompleted(other.GetComponent<Objective>());
                                Destroy(other.gameObject);
                            }
                            else
                            {
                                Debug.Log("Player: Prerequierement Objectives not completed");
                                gm.Infotext = "Prerequierement Objectives not completed";
                            }
                        }
                        break;
                    }
                case "CaveEntrance(Clone)":
                    {
                        if (gm != null)
                            //gm.SendMessage("InitiateLevelTransition", "Cave");
                            gm.InitiateLevelTransition("Cave", this.transform.position);
                        break;
                    }
                case "DungeonEntrance(Clone)":
                    {
                        if (gm != null)
                            //gm.SendMessage("InitiateLevelTransition", "Dungeon");
                            gm.InitiateLevelTransition("Dungeon", this.transform.position);
                        break;
                    }
                
            }

        }
    }

    /// <summary>
    /// Moves player to a different position
    /// </summary>
    /// <param name="pos">position</param>
    public void MovePlayer(Vector3 pos)
    {
        this.transform.position = pos;
    }
}
