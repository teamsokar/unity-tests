﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

/// <summary>
/// Lindenmayer-System
/// </summary>
public class LSystem{
    private Dictionary<string, string> m_variables;
    public Dictionary<string, string> Variables
    {
        get { return m_variables; }
        set { m_variables = value; }
    }

    private Dictionary<string, string> m_constants;
    public Dictionary<string, string> Constants
    {
        get { return m_constants; }
        set { m_constants = value; }
    }

    private Dictionary<string, LSystemProductionRule> m_productionRules;

    // difficulty settings, sets number of iterations in mission production
    public enum difficulty { easy, normal, hard };
    private difficulty m_difficulty = difficulty.normal;
    public difficulty Difficulty
    {
        get { return m_difficulty; }
        set { m_difficulty = value; }
    }

    private int m_numberOfObjectives = 3;

    public int NumberOfObjectives
    {
        get { return m_numberOfObjectives; }
        set { m_numberOfObjectives = value; }
    }

    /// <summary>
    /// basic constructor, initialize members with default values for testing
    /// </summary>
    public LSystem()
    {
        m_constants = new Dictionary<string, string>();
        // Start and End to support multiple objectives in one mission
        m_constants.Add("Start", "S");
        m_constants.Add("End", "E");

        m_constants.Add("Key", "K");
        m_constants.Add("Chest", "C");

        m_constants.Add("Find", "F");

        m_constants.Add("Enemy", "Ke"); //KillEnemy
        //m_constants.Add("Boss", "Kb");  // KillBoss - not implemented as it will overlap with KillEnemy

        m_constants.Add("Overworld Dungeon", "Od");
        m_constants.Add("Overworld Cave", "Oc");

        m_variables = new Dictionary<string, string>();
        m_variables.Add("Location", "L");
        m_variables.Add("Objective", "O");
        m_variables.Add("Replacer", "A");

        m_productionRules = new Dictionary<string, LSystemProductionRule>();
        //REFACTOR better identifier for entries
        m_productionRules.Add("removeReplacer",   new LSystemProductionRule(m_variables["Replacer"],  string.Empty));
        m_productionRules.Add("removeLocation",   new LSystemProductionRule(m_variables["Location"],  string.Empty));

        m_productionRules.Add("FindKeysAndChest", new LSystemProductionRule(m_variables["Objective"], m_variables["Replacer"] + " " +  m_constants["Chest"]));
        m_productionRules.Add("keychest1",        new LSystemProductionRule(m_variables["Replacer"],  m_variables["Replacer"] + " " + m_constants["Key"]));
        m_productionRules.Add("keychest2",        new LSystemProductionRule(m_variables["Replacer"],  m_constants["Key"]));

        m_productionRules.Add("FindStuff",        new LSystemProductionRule(m_variables["Objective"], m_variables["Replacer"]));
        m_productionRules.Add("find1",            new LSystemProductionRule(m_variables["Replacer"],  m_variables["Replacer"] + " " + m_constants["Find"]));
        m_productionRules.Add("find2",            new LSystemProductionRule(m_variables["Replacer"],  m_constants["Find"]));

        m_productionRules.Add("Enemy",            new LSystemProductionRule(m_variables["Objective"], m_variables["Replacer"]));
        m_productionRules.Add("kill1",            new LSystemProductionRule(m_variables["Replacer"],  m_variables["Replacer"] + " " + m_constants["Enemy"]));
        m_productionRules.Add("kill2",            new LSystemProductionRule(m_variables["Replacer"],  m_constants["Enemy"]));

        m_productionRules.Add("EnterLocation",    new LSystemProductionRule(m_variables["Location"],  m_variables["Location"] + " " + m_variables["Replacer"]));
        m_productionRules.Add("enterDungeon",     new LSystemProductionRule(m_variables["Replacer"],  m_constants["Overworld Dungeon"]));
        m_productionRules.Add("enterCave",        new LSystemProductionRule(m_variables["Replacer"],  m_constants["Overworld Cave"]));
   }

    /// <summary>
    /// Starts production
    /// </summary>
    /// <param name="random">true to randomize seeds</param>
    /// <param name="overworld">specifies if the current scene is the overworld</param>
    /// <returns></returns>
    public string StartProduction(bool random = true, bool overworld = false)
    {
        int[] iterations;

        // set number of objectives
        if (!overworld)
        {
            iterations = new int[3] { 3, 5, 7 }; 
        }
        else
        {
            iterations = new int[3] { 1, 2, 3 };
        }

        switch (m_difficulty)
        {
            case difficulty.easy:
                {
                    return Produce(iterations[0], random, overworld);
                }
            case difficulty.normal:
                {
                    return Produce(iterations[1], random, overworld);
                }
            case difficulty.hard:
                {
                    return Produce(iterations[2], random, overworld);
                }
        }
        
        throw new System.ArgumentException("no valid difficulty selected");
    }

    /// <summary>
    /// Production of rules
    /// </summary>
    /// <param name="iterations">Number of iterations in production</param>
    /// <param name="random">true to randomize seeds</param>
    /// <param name="overworld">specifies if the current scene is the overworld</param>
    /// <returns>produced missions</returns>
    public string Produce(int iterations, bool random = true, bool overworld = false)
    {
        string m = "";
        if (!overworld)
        {
            m = m_constants["Start"] + " " + m_variables["Objective"] + " " + m_constants["End"];
        }
        else
        {
            m = m_constants["Start"] + " " + m_variables["Location"] + " " + m_constants["End"];
        }
        string subtask = "";

        int objective = 0;


        if (!overworld)
        {
            if (random) objective = UnityEngine.Random.Range(0, m_numberOfObjectives); // probably the only time the inclusive/exclusive definition has actually an advantage...

            // only works with one objective per level, if multiple it should be similar to overworld location generation
            switch (objective)
            {
                case 0:
                    m = m_productionRules["FindKeysAndChest"].ApplyRule(m);
                    subtask = "keychest1";
                    break;

                case 1:
                    m = m_productionRules["FindStuff"].ApplyRule(m);
                    subtask = "find1";
                    break;

                case 2:
                    m = m_productionRules["Enemy"].ApplyRule(m);
                    subtask = "kill1";
                    break;

                default:
                    throw new System.ArgumentException("LSystem: invalid number of objective");
            }
        }

        for (int i = 0; i < iterations; i++)
        {
            if (overworld)
            {
                m = m_productionRules["EnterLocation"].ApplyRule(m);
                if (UnityEngine.Random.Range(0, 2) == 0)
                {
                    subtask = "enterDungeon";
                }
                else
                {
                    subtask = "enterCave";
                }
            }

            m = m_productionRules[subtask].ApplyRule(m); // uses replace function which replaces ALL occurences, therefore production one after another
        }

        // end  
        if (!overworld)
        {
            m = m_productionRules["removeReplacer"].ApplyRule(m); 
        }
        else
        {
            m = m_productionRules["removeLocation"].ApplyRule(m);
        }

        return m;
    }
}

/// <summary>
/// class defining a production rule
/// </summary>
public class LSystemProductionRule
{
    private string m_left { get; set; }
    private string m_right{ get; set; }

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="l">variables to be replaced</param>
    /// <param name="r">variables and/or constants that will replace</param>
    public LSystemProductionRule(string l, string r)
    {
        m_left = l;
        m_right = r;
    }

    /// <summary>
    /// Applies a rule to a string.
    /// Uses C# replace function and therefore replaces ALL occurences - use with caution!
    /// Make sure that there is only one symbol of a variable to be replaced appearing in the string
    /// </summary>
    /// <param name="state">current state of production</param>
    /// <returns>new state of production after applying rule</returns>
    public string ApplyRule(string state)
    {
        return state.Replace(this.m_left, this.m_right);
    }
}
