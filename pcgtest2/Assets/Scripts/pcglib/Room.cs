﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class that defindes a room in a dungeon.
/// Rooms are 2d-rectangles that are defined by its lower left and upper right point.
/// Center point is used to calculate a Relative Neighbourhood Graph, which is used to connect rooms.
/// Connected rooms are defined as "neighbours"
/// </summary>
public class Room{

    /// <summary>
    /// lower left point of room
    /// </summary>
    private int[] m_lowerLeft;

    public int[] LowerLeft
    {
        get { return m_lowerLeft; }
        set { m_lowerLeft = value; }
    }

    public int LowerLeftX
    {
        get { return m_lowerLeft[0]; }
        set { m_lowerLeft[0] = value; }
    }

    public int LowerLeftY
    {
        get { return m_lowerLeft[1]; }
        set { m_lowerLeft[0] = value; }
    }

    /// <summary>
    /// upper right point of room
    /// </summary>
    private int[] m_upperRight;

    public int[] UpperRight
    {
        get { return m_upperRight; }
        set { m_upperRight = value; }
    }

    public int UpperRightX
    {
        get { return m_upperRight[0]; }
        set { m_upperRight[0] = value; }
    }

    public int UpperRightY
    {
        get { return m_upperRight[1]; }
        set { m_upperRight[1] = value; }
    }

    /// <summary>
    /// center point of room
    /// </summary>
    private int[] m_center;

    public int[] Center
    {
        get { return m_center; }
        set { m_center = value; }
    }

    public int CenterX
    {
        get { return m_center[0]; }
        set { m_center[0] = value; }
    }

    public int CenterY
    {
        get { return m_center[1]; }
        set { m_center[1] = value; }
    }

    /// <summary>
    /// List of neighbours: rooms that are connected to the current one
    /// </summary>
    private ArrayList m_neighbours;

    public ArrayList Neighbours
    {
        get { return m_neighbours; }
        set { m_neighbours = value; }
    }

  
    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="lowerLeftX">x coordinate of lower left</param>
    /// <param name="lowerLeftY">y coordinate of lower left</param>
    /// <param name="upperRightX">x coordinate of upper right</param>
    /// <param name="upperRightY">y coordinate of upper right</param>
    public Room(int lowerLeftX, int lowerLeftY, int upperRightX, int upperRightY)
    {
        m_lowerLeft = new int[2];
        m_lowerLeft[0] = lowerLeftX;
        m_lowerLeft[1] = lowerLeftY;

        m_upperRight = new int[2];
        m_upperRight[0] = upperRightX;
        m_upperRight[1] = upperRightY;

        m_center = new int[2];
        m_center[0] = lowerLeftX + (upperRightX - lowerLeftX) / 2;
        m_center[1] = lowerLeftY + (upperRightY - lowerLeftY) / 2;

        m_neighbours = new ArrayList();
    }

    /// <summary>
    /// Generates list of neighbours based on the Relative Neighbour Graph approach
    /// self developed, naiv approach based on properties of RNG
    /// OPTIONAL does not work properly
    /// </summary>
    /// <param name="rooms">List of rooms</param>
    public static void CalculateRelativeNeighbours(List<Room> rooms){
        foreach (Room p in rooms)
        {
            foreach (Room q in rooms)
            {
                if (p == q) continue;

                float dist = EuclideanDistance(p.CenterX, p.CenterY, q.CenterX, q.CenterY);
                bool closerRFound = false;
                foreach (Room r in rooms)
                {
                    if (r == p || r == q) continue;

                                            // just to show that both variants are possible
                    if (EuclideanDistance(p.CenterX, p.CenterY, r.CenterX, r.CenterY) < dist || EuclideanDistance(q.Center, r.Center) < dist)
                    {
                        closerRFound = true;
                        break;
                    }
                }

                if (!closerRFound)
                {
                    if (!p.m_neighbours.Contains(q)) // avoid duplicate entries
                        p.m_neighbours.Add(q);

                    if (!q.m_neighbours.Contains(p)) // avoid duplicate entries
                         q.m_neighbours.Add(p);
                }
            }
        }
    }

    /// <summary>
    /// Generates list of neighbours based on the Relative Neighbour Graph approach
    /// naive approach, complexity O(n^3)
    /// Paper: http://www.cs.unsyiah.ac.id/~frdaus/PenelusuranInformasi/tugas2/data/relng.pdf
    /// </summary>
    /// <param name="rooms">List of rooms</param>
    public static void CalculateRNG(List<Room> rooms)
    {
        List<Vector3> roomsDistances = new List<Vector3>(); // [pi, pj, dist(pi,pj)]
        for (int i = 0; i < rooms.Count; i++)
        {
            for (int j = 0; j < rooms.Count; j++)
            {
                if (i == j) continue;

                roomsDistances.Add(new Vector3(i,j,EuclideanDistance(rooms[i].CenterX, rooms[i].CenterY, rooms[j].CenterX, rooms[j].CenterY)));
            }
        }

        List<Vector4> dkmax = new List<Vector4>(); //[pi,pj,pk,max(dist(pi,pk),dist(pj,pk)]

        foreach(Vector4 d in roomsDistances){
            for(int k = 0 ;k < rooms.Count; k++)
            {
                if (d.x == k || d.y == k) continue;

                float max = Mathf.Max(EuclideanDistance(rooms[k].CenterX, rooms[k].CenterY, rooms[(int)d.x].CenterX, rooms[(int)d.x].CenterY),
                                      EuclideanDistance(rooms[k].CenterX, rooms[k].CenterY, rooms[(int)d.y].CenterX, rooms[(int)d.y].CenterY));

                dkmax.Add(new Vector4(d.x, d.y, k, max));
            }
        }

        bool found = false;

        for (int i = 0; i < rooms.Count; i++)
        {
            for (int j = 0; j < rooms.Count; j++)
            {
                if (i == j) continue;

                found = false;

                foreach (Vector3 d in roomsDistances)
                {
                    if (d.x == i && d.y == j)
                    {
                        foreach (Vector4 dk in dkmax)
                        {

                            if (dk.x == i && dk.y == j && !found)
                            {
                                if (dk.w < d.z)
                                {
                                    found = true;                                    
                                }
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                    else
                    {
                        continue;
                    }
                }

                if (!found)
                {
                    if (!rooms[i].m_neighbours.Contains(rooms[j]))
                        rooms[i].m_neighbours.Add(rooms[j]);

                    if (!rooms[j].m_neighbours.Contains(rooms[i]))
                        rooms[j].m_neighbours.Add(rooms[i]);
                }
            }
        }
    }

    /// <summary>
    /// Calculates euclidian distance between two points defined as Vector2 objects
    /// </summary>
    /// <param name="p">first point</param>
    /// <param name="q">second point</param>
    /// <returns>distance</returns>
    public static float EuclideanDistance(Vector2 p, Vector2 q)
    {
        return Mathf.Abs(Mathf.Sqrt((q.x - p.x) * (q.x - p.x) + (q.y - p.y) * (q.y - p.y)));
    }

    /// <summary>
    /// Calcualtes euclidian distance between two points defined as two pairs of int values
    /// </summary>
    /// <param name="x1">x coordinate of first point</param>
    /// <param name="y1">y coordinate of first point</param>
    /// <param name="x2">x coordinate of second point</param>
    /// <param name="y2">y coordinate of second point</param>
    /// <returns>distance</returns>
    public static float EuclideanDistance(int x1, int y1, int x2, int y2)
    {
        return Mathf.Abs(Mathf.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)));
    }

    /// <summary>
    /// Calcualtes euclidian distance between two points defined as two pairs of float values
    /// </summary>
    /// <param name="x1">x coordinate of first point</param>
    /// <param name="y1">y coordinate of first point</param>
    /// <param name="x2">x coordinate of second point</param>
    /// <param name="y2">y coordinate of second point</param>
    /// <returns>distance</returns>
    public static float EuclideanDistance(float x1, float y1, float x2, float y2)
    {
        //return EuclideanDistance(new Vector2(x1, y1), new Vector2(x2, y2));
        return Mathf.Abs(Mathf.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)));
    }

    /// <summary>
    /// Calculates euclidian distance between two points defined as Vector2 objects
    /// </summary>
    /// <param name="p">first point</param>
    /// <param name="q">second point</param>
    /// <returns>distance</returns>
    public static float EuclideanDistance(int[] p, int[] q)
    {
        return Mathf.Abs(Mathf.Sqrt((q[0] - p[0]) * (q[0] - p[0]) + (q[1] - p[1]) * (q[1] - p[1])));
    }

}
