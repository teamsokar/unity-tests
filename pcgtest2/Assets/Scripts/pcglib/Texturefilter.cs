﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.IO;

// REFACTOR declare functions public static
public class Texturefilter : MonoBehaviour {

    public enum FilterMode { median, gaussian };
    public FilterMode filterMode = FilterMode.median;
    public int medianFilterMatrixSize = 7;
    public int filterOffset = 0;
    public int filterFactor = 1;

    public enum FilterMatrix { gaussian3x3, gaussian5x5 }
    //FilterMatrix filterMatrix = FilterMatrix.gaussian3x3;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// Filter texture with median filter
    /// adapted from http://en.wikipedia.org/wiki/Median_filter#2D_median_filter_pseudo_code
    /// </summary>
    /// <param name="sourceTexture">unfiltered texture</param>
    /// <param name="windowWidth">width of window</param>
    /// <param name="windowHeight">height of window</param>
    /// <returns>filtered texture</returns>
    public static Texture2D MedianFilter(Texture2D sourceTexture, int windowWidth, int windowHeight)
    {
        int edgeX = (int)Mathf.Floor((windowWidth / 2));
        int edgeY = (int)Mathf.Floor((windowHeight / 2));
        Texture2D resultTexture = new Texture2D(sourceTexture.width, sourceTexture.height, TextureFormat.RGB24, false);
        List<Color> window = new List<Color>();

        for (int x = edgeX; x < sourceTexture.width - edgeX; x++)
        {
            for (int y = edgeY; y < sourceTexture.height - edgeY; y++)
            {
                window.Clear();
                for (int fx = 0; fx < windowWidth; fx++)
                {
                    for (int fy = 0; fy < windowHeight; fy++)
                    {
                        window.Add(sourceTexture.GetPixel(x + fx - edgeX, y + fy - edgeY));
                    }
                }
                window = window.OrderBy(Color => Color.grayscale).ToList();
                resultTexture.SetPixel(x, y, window[(int)((windowWidth * windowHeight) / 2)]);
            }
        }

        return resultTexture;
    }

    /// <summary>
    /// filter 2d texture in form of a 2d float array
    /// adapted from http://en.wikipedia.org/wiki/Median_filter#2D_median_filter_pseudo_code
    /// </summary>
    /// <param name="source"></param>
    /// <param name="windowWidth"></param>
    /// <param name="windowHeight"></param>
    /// <returns></returns>
    public static float[,] MedianFilter(float[,] source, int windowWidth, int windowHeight)
    {
        int edgeX = (int)Mathf.Floor(windowWidth / 2);
        int edgeY = (int)Mathf.Floor(windowHeight / 2);
        float[,] result = new float[source.GetLength(0), source.GetLength(1)];
        List<float> window = new List<float>();

        for (int x = edgeX; x < source.GetLength(0) - edgeX; x++)
        {
            for (int y = edgeY; y < source.GetLength(1) - edgeY; y++)
            {
                window.Clear();
                for (int fx = 0; fx < windowWidth; fx++)
                {
                    for (int fy = 0; fy < windowHeight; fy++)
                    {
                        window.Add(source[x + fx - edgeX, y + fy - edgeY]);
                    }
                }
                window = window.OrderBy(item => item).ToList();
                result[x,y] = window[(int)(windowHeight*windowHeight/2)];
            }
        }


        return result;
    }

    // adapted from: http://studentguru.gr/b/jupiter/archive/2009/10/14/creating-an-image-processing-library-with-c-part-1
    public static Texture2D ConvolutionFilter(Texture2D sourceTexture, float[,] filterMatrix, float factor = 1, int filterOffset = 0)
    {
        if (factor == 0)
        {
            throw new ArgumentException("Factor is not allowed to be 0");
        }

        Texture2D resultTexture = new Texture2D(sourceTexture.width, sourceTexture.height, TextureFormat.RGB24, false);

        int s = (filterMatrix.GetLength(0) + filterMatrix.GetLength(1)) / 2;
        float r = 0.0f, b = 0.0f, g = 0.0f;
        Color tempPix;

        for (int x = s; x < sourceTexture.width; x++)
        {
            for (int y = s; y < sourceTexture.height; y++)
            {
                r = b = g = 0.0f;
                for (int filterX = 0; filterX < filterMatrix.GetLength(1); filterX++)
                {
                    for (int filterY = 0; filterY < filterMatrix.GetLength(0); filterY++)
                    {
                        tempPix = sourceTexture.GetPixel(x + filterX - s, y + filterY - s);
                        r += filterMatrix[filterX, filterY] * tempPix.r;
                        g += filterMatrix[filterX, filterY] * tempPix.g;
                        b += filterMatrix[filterX, filterY] * tempPix.b;

                    }
                }

                r = Mathf.Min(Mathf.Max((r / factor) + filterOffset, 0.0f), 1.0f);
                g = Mathf.Min(Mathf.Max((g / factor) + filterOffset, 0.0f), 1.0f);
                b = Mathf.Min(Mathf.Max((b / factor) + filterOffset, 0.0f), 1.0f);

                resultTexture.SetPixel(x, y, new Color(r, g, b));
            }
        }


        return resultTexture;
    }

    // adapted from: http://studentguru.gr/b/jupiter/archive/2009/10/14/creating-an-image-processing-library-with-c-part-1
    public static float[,] ConvolutionFilter(float[,] sourceTexture, float[,] filterMatrix, float factor = 1, int filterOffset = 0)
    {
        if (factor == 0)
        {
            throw new ArgumentException("Factor is not allowed to be 0");
        }

                float[,] result = new float[sourceTexture.GetLength(0), sourceTexture.GetLength(1)];

        int s = (filterMatrix.GetLength(0) + filterMatrix.GetLength(1)) / 2;
        float r = 0.0f /*, b = 0.0f, g = 0.0f*/;
        float tempVal;

        for (int x = s; x < sourceTexture.GetLength(0); x++)
        {
            for (int y = s; y < sourceTexture.GetLength(1); y++)
            {
                r = 0.0f;
                for (int filterX = 0; filterX < filterMatrix.GetLength(1); filterX++)
                {
                    for (int filterY = 0; filterY < filterMatrix.GetLength(0); filterY++)
                    {
                        tempVal = sourceTexture[x + filterX - s, y + filterY - s];
                        r += filterMatrix[filterX, filterY] * tempVal;
                    }
                }

                r = Mathf.Min(Mathf.Max((r / factor) + filterOffset, 0.0f), 1.0f);

                result[x, y] = r;
            }
        }

        return result;
    }

    // temporary hack
    public static Texture2D ConvolutionFilter(Texture2D sourceTexture, FilterMatrix fm, float factor = 1, int filterOffset = 0)
    {
        Texture2D resultTexture = new Texture2D(sourceTexture.width, sourceTexture.height, TextureFormat.RGB24, false);

        switch (fm)
        {
            case FilterMatrix.gaussian3x3: resultTexture = ConvolutionFilter(sourceTexture, FilterMatrizes.gaussian3x3, factor, filterOffset);
                break;
            case FilterMatrix.gaussian5x5: resultTexture = ConvolutionFilter(sourceTexture, FilterMatrizes.gaussian5x5, factor, filterOffset);
                break;
            default: throw new ArgumentException("unknown filter type");
        }

        return resultTexture;
    }

    public Texture2D GenerateIslandsMap(int width, int height, float[,] filterMatrix, int count)
    {
        Texture2D islandmap = new Texture2D(width, height, TextureFormat.RGB24, false);
        float r = 0.0f;
        float b = 0.0f;
        float g = 0.0f;
        Color tempPixel;

        for (int i = 0; i < count; i++)
        {
            int posX = UnityEngine.Random.Range(Mathf.FloorToInt(filterMatrix.GetLength(0) / 2.0f), width - Mathf.FloorToInt(filterMatrix.GetLength(0) / 2.0f));
            int posY = UnityEngine.Random.Range(Mathf.FloorToInt(filterMatrix.GetLength(1) / 2.0f), width - Mathf.FloorToInt(filterMatrix.GetLength(1) / 2.0f));
            int minX = posX - Mathf.FloorToInt(filterMatrix.GetLength(0) / 2.0f);
            int minY = posY - Mathf.FloorToInt(filterMatrix.GetLength(1) / 2.0f);

            r = b = g = 0.0f;

            for (int filterX = 0; filterX < filterMatrix.GetLength(0); filterX++)
            {
                for (int filterY = 0; filterY < filterMatrix.GetLength(1); filterY++)
                {
                    tempPixel = islandmap.GetPixel(filterX, filterY);
                    r += filterMatrix[filterX, filterY] * tempPixel.r;
                    b += filterMatrix[filterX, filterY] * tempPixel.b;
                    g += filterMatrix[filterX, filterY] * tempPixel.g;
                    islandmap.SetPixel(minX + filterX, minY + filterY, tempPixel);

                }
            }


        }
#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("islandmap.png", islandmap.EncodeToPNG());
#endif

            return islandmap;
    }

    public Texture2D GenerateIslandsOnNoiseMap(Texture2D source, float[,] filterMatrix, int count)
    {
        Texture2D temptex = source;

        float r = 0.0f;
        float b = 0.0f;
        float g = 0.0f;
        Color tempPixel;

        for (int i = 0; i < count; i++)
        {
            int posX = UnityEngine.Random.Range(Mathf.FloorToInt(filterMatrix.GetLength(0) / 2.0f), source.width - Mathf.FloorToInt(filterMatrix.GetLength(0) / 2.0f));
            int posY = UnityEngine.Random.Range(Mathf.FloorToInt(filterMatrix.GetLength(1) / 2.0f), source.width - Mathf.FloorToInt(filterMatrix.GetLength(1) / 2.0f));
            int minX = posX - Mathf.FloorToInt(filterMatrix.GetLength(0) / 2.0f);
            int minY = posY - Mathf.FloorToInt(filterMatrix.GetLength(1) / 2.0f);

            r = b = g = 0.0f;

            for (int filterX = 0; filterX < filterMatrix.GetLength(0); filterX++)
            {
                for (int filterY = 0; filterY < filterMatrix.GetLength(1); filterY++)
                {
                    tempPixel = source.GetPixel(filterX, filterY);
                    r += filterMatrix[filterX, filterY] * tempPixel.r;
                    b += filterMatrix[filterX, filterY] * tempPixel.b;
                    g += filterMatrix[filterX, filterY] * tempPixel.g;
                    temptex.SetPixel(minX + filterX, minY + filterY, tempPixel);

                }
            }
        }

#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("islandmapOnNoise.png", temptex.EncodeToPNG());
#endif
        return temptex;
    }

    public Texture2D GenerateIslandsFilterMask(int width, int height, float[,] filterMatrix, int count)
    {
        Texture2D mask = new Texture2D(width, height, TextureFormat.RGB24, false);
        int posX = 0, posY = 0;
        Color tempPixel;
        float r = 0.0f, b = 0.0f, g = 0.0f; ;

        for (int i = 0; i < count; i++)
        {
            posX = UnityEngine.Random.Range(Mathf.FloorToInt(filterMatrix.GetLength(0) / 2.0f), width - Mathf.FloorToInt(filterMatrix.GetLength(0) / 2.0f));
            posY = UnityEngine.Random.Range(Mathf.FloorToInt(filterMatrix.GetLength(1) / 2.0f), width - Mathf.FloorToInt(filterMatrix.GetLength(1) / 2.0f));
            //int boundUpperX = posX - Mathf.FloorToInt(filterMatrix.GetLength(0) / 2.0f);
            int boundLowerX = posX - Mathf.FloorToInt(filterMatrix.GetLength(0) / 2.0f);
            //int boundUpperY = posY - Mathf.FloorToInt(filterMatrix.GetLength(1) / 2.0f);
            int boundLowerY = posY - Mathf.FloorToInt(filterMatrix.GetLength(1) / 2.0f);

            r = b = g = 0.0f;

            for (int filterX = 0; filterX < filterMatrix.GetLength(0); filterX++)
            {
                for (int filterY = 0; filterY < filterMatrix.GetLength(1); filterY++)
                {
                    tempPixel = mask.GetPixel(boundLowerX + filterX, boundLowerY + filterY);
                    r += filterMatrix[filterX, filterY] * tempPixel.r;
                    b += filterMatrix[filterX, filterY] * tempPixel.b;
                    g += filterMatrix[filterX, filterY] * tempPixel.g;
                    mask.SetPixel(boundLowerX + filterX, boundLowerY + filterY, new Color(r, b, g));
                }
            }
        }

#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("mask_islands.png", mask.EncodeToPNG());
#endif

        return mask;
    }

    public Texture2D FilterNoiseTextureWithMask(Texture2D source, Texture2D mask)
    {
        if (source.width != mask.width) throw new ArgumentException("Filter with Mask: widths of textures are not equal");
        if (source.height != mask.height) throw new ArgumentException("Filter with Mask: heights of textures are not equal");

        //Texture2D tempTex = new Texture2D(source.width, source.height, TextureFormat.RGB24, false);

        //temporary hack; generate from texture? supply via parameter?
        Color keyColor = mask.GetPixel(0,0);

        for (int x = 0; x < source.width; x++)
        {
            for (int y = 0; y < source.height; y++)
            {
                if(mask.GetPixel(x,y) == keyColor){
                    source.SetPixel(x, y, Color.black);
                    //tempTex.SetPixel(x, y, Color.black);
                }
            }
        }

#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("noisetexture_maskfiltered.png", source.EncodeToPNG());
#endif

        return source;
    }
}

public class FilterMatrizes{
    // filter matrizes for convolution filter
    // generation: http://www.embege.com/gauss/
    public static float[,] gaussian3x3 = {
                                     {0.14676266317374237f, 0.24197072451914536f, 0.14676266317374237f}, 
                                     {0.24197072451914536f, 0.3989422804014327f, 0.24197072451914536f}, 
                                     {0.14676266317374237f, 0.24197072451914536f, 0.14676266317374237f}
                                 };

    public static float[,] gaussian5x5 = {
                                       {0.0073068827452812644f, 0.03274717653776802f, 0.05399096651318985f, 0.03274717653776802f, 0.0073068827452812644f}, 
                                       {0.03274717653776802f, 0.14676266317374237f, 0.24197072451914536f, 0.14676266317374237f, 0.03274717653776802f}, 
                                       {0.05399096651318985f, 0.24197072451914536f, 0.3989422804014327f, 0.24197072451914536f, 0.05399096651318985f}, 
                                       {0.03274717653776802f, 0.14676266317374237f, 0.24197072451914536f, 0.14676266317374237f, 0.03274717653776802f}, 
                                       {0.0073068827452812644f, 0.03274717653776802f, 0.05399096651318985f, 0.03274717653776802f, 0.0073068827452812644f}
                                   };

    public static float[,] gaussian10x10 = {
                                                {5.540487995578148e-12f, 4.987388353668856e-10f, 1.651596194252529e-8f, 2.0120560662001434e-7f, 9.017409680796356e-7f, 0.0000014867195147346085f, 2.0120560662001434e-7f, 1.651596194252529e-8f, 4.987388353668856e-10f, 5.540487995578148e-12f}, 
                                                {4.987388353668856e-10f, 4.489503922788689e-8f, 0.0000014867195147346085f, 0.00001811195150951361f, 0.00008117213512267813f, 0.00013383022576490326f, 0.00001811195150951361f, 0.0000014867195147346085f, 4.489503922788689e-8f, 4.987388353668856e-10f}, 
                                                {1.651596194252529e-8f, 0.0000014867195147346085f, 0.00004923338866624139f, 0.0005997854600914276f, 0.002688051941039371f, 0.004431848411938341f, 0.0005997854600914276f, 0.00004923338866624139f, 0.0000014867195147346085f, 1.651596194252529e-8f}, 
                                                {2.0120560662001434e-7f, 0.00001811195150951361f, 0.0005997854600914276f, 0.0073068827452812644f, 0.03274717653776802f, 0.05399096651318985f, 0.0073068827452812644f, 0.0005997854600914276f, 0.00001811195150951361f, 2.0120560662001434e-7f}, 
                                                {9.017409680796356e-7f, 0.00008117213512267813f, 0.002688051941039371f, 0.03274717653776802f, 0.14676266317374237f, 0.24197072451914536f, 0.03274717653776802f, 0.002688051941039371f, 0.00008117213512267813f, 9.017409680796356e-7f}, 
                                                {0.0000014867195147346085f, 0.00013383022576490326f, 0.004431848411938341f, 0.05399096651318985f, 0.24197072451914536f, 0.3989422804014327f, 0.24197072451914536f, 0.05399096651318985f, 0.004431848411938341f, 0.00013383022576490326f}, 
                                                {2.0120560662001434e-7f, 0.00001811195150951361f, 0.0005997854600914276f, 0.0073068827452812644f, 0.03274717653776802f, 0.24197072451914536f, 0.0073068827452812644f, 0.0005997854600914276f, 0.00001811195150951361f, 2.0120560662001434e-7f}, 
                                                {1.651596194252529e-8f, 0.0000014867195147346085f, 0.00004923338866624139f, 0.0005997854600914276f, 0.002688051941039371f, 0.05399096651318985f, 0.0005997854600914276f, 0.00004923338866624139f, 0.0000014867195147346085f, 1.651596194252529e-8f}, 
                                                {4.987388353668856e-10f, 4.489503922788689e-8f, 0.0000014867195147346085f, 0.00001811195150951361f, 0.00008117213512267813f, 0.004431848411938341f, 0.00001811195150951361f, 0.0000014867195147346085f, 4.489503922788689e-8f, 4.987388353668856e-10f}, 
                                                {5.540487995578148e-12f, 4.987388353668856e-10f, 1.651596194252529e-8f, 2.0120560662001434e-7f, 9.017409680796356e-7f, 0.00013383022576490326f, 2.0120560662001434e-7f, 1.651596194252529e-8f, 4.987388353668856e-10f, 5.540487995578148e-12f}
                                           };

    // normalized, o^2 = 50
    /*
    public static float[,] gaussian7x7 = { 
        {0.04712507527259733f, 0.04954122954862601f, 0.051049984606013586f, 0.05156304548094822f, 0.051049984606013586f, 0.04954122954862601f, 0.04712507527259733f}, 
        {0.04954122954862601f, 0.0520812627034003f, 0.05366737328674301f, 0.054206739355243186f, 0.05366737328674301f, 0.0520812627034003f, 0.04954122954862601f}, 
        {0.051049984606013586f, 0.05366737328674301f, 0.05530178812870016f, 0.05585758033944685f, 0.05530178812870016f, 0.05366737328674301f, 0.051049984606013586f}, 
        {0.05156304548094822f, 0.054206739355243186f, 0.05585758033944685f, 0.056418958354775624f, 0.05585758033944685f, 0.054206739355243186f, 0.05156304548094822f}, 
        {0.051049984606013586f, 0.05366737328674301f, 0.05530178812870016f, 0.05585758033944685f, 0.05530178812870016f, 0.05366737328674301f, 0.051049984606013586f}, 
        {0.04954122954862601f, 0.0520812627034003f, 0.05366737328674301f, 0.054206739355243186f, 0.05366737328674301f, 0.0520812627034003f, 0.04954122954862601f}, 
        {0.04712507527259733f, 0.04954122954862601f, 0.051049984606013586f, 0.05156304548094822f, 0.051049984606013586f, 0.04954122954862601f, 0.04712507527259733f}
    };*/
    public static float[,] gaussian7x7 = {
                    {0.11467427226951498f, 0.26386273579782998f, 0.4350361050550067f, 0.5139344326792439f, 0.4350361050550067f, 0.26386273579782998f, 0.11467427226951498f}, 
                    {0.26386273579782998f, 0.6071417935758214f, 0.10010075893994747f, 0.11825507390946054f, 0.10010075893994747f, 0.6071417935758214f, 0.26386273579782998f}, 
                    {0.4350361050550067f, 0.10010075893994747f, 0.16503825047751605f, 0.1949696557227417f, 0.16503825047751605f, 0.10010075893994747f, 0.4350361050550067f}, 
                    {0.5139344326792439f, 0.11825507390946054f, 0.1949696557227417f, 0.23032943298089034f, 0.1949696557227417f, 0.11825507390946054f, 0.5139344326792439f}, 
                    {0.4350361050550067f, 0.10010075893994747f, 0.16503825047751605f, 0.1949696557227417f, 0.16503825047751605f, 0.10010075893994747f, 0.4350361050550067f}, 
                    {0.26386273579782998f, 0.6071417935758214f, 0.10010075893994747f, 0.11825507390946054f, 0.10010075893994747f, 0.6071417935758214f, 0.26386273579782998f}, 
                    {0.11467427226951498f, 0.26386273579782998f, 0.4350361050550067f, 0.5139344326792439f, 0.4350361050550067f, 0.26386273579782998f, 0.11467427226951498f}
                                          };

    // o^2: 75
    public static float [,] gaussian20x20 = { 
                    {0.012142835871516428f, 0.013782587531019143f, 0.01543656978415747f, 0.017060048000296084f, 0.018604547169933296f, 0.020020151787989493f, 0.02125812883587265f, 0.02227368690486281f, 0.023028656050868932f, 0.023493865762111734f, 0.023651014781892105f, 0.023028656050868932f, 0.02227368690486281f, 0.02125812883587265f, 0.020020151787989493f, 0.018604547169933296f, 0.017060048000296084f, 0.01543656978415747f, 0.013782587531019143f, 0.012142835871516428f}, 
                    {0.013782587531019143f, 0.015643768972929527f, 0.017521102688038404f, 0.01936381314343707f, 0.02111687933179302f, 0.022723645227677437f, 0.024128796973478923f, 0.02528149459097062f, 0.026138413719924366f, 0.026666444703240564f, 0.026844814907981403f, 0.026138413719924366f, 0.02528149459097062f, 0.024128796973478923f, 0.022723645227677437f, 0.02111687933179302f, 0.01936381314343707f, 0.017521102688038404f, 0.015643768972929527f, 0.013782587531019143f}, 
                    {0.01543656978415747f, 0.017521102688038404f, 0.019623726221987173f, 0.02168757152481869f, 0.023651014781892105f, 0.025450600949787093f, 0.02702437822002595f, 0.02831540555233173f, 0.029275159437710887f, 0.029866556887849564f, 0.0300663324457192f, 0.029275159437710887f, 0.02831540555233173f, 0.02702437822002595f, 0.025450600949787093f, 0.023651014781892105f, 0.02168757152481869f, 0.019623726221987173f, 0.017521102688038404f, 0.01543656978415747f}, 
                    {0.017060048000296084f, 0.01936381314343707f, 0.02168757152481869f, 0.023968473332915102f, 0.026138413719924366f, 0.0281272640172531f, 0.029866556887849564f, 0.031293362749954694f, 0.03235405483258585f, 0.03300765009550321f, 0.03322843623220306f, 0.03235405483258585f, 0.031293362749954694f, 0.029866556887849564f, 0.0281272640172531f, 0.026138413719924366f, 0.023968473332915102f, 0.02168757152481869f, 0.01936381314343707f, 0.017060048000296084f}, 
                    {0.018604547169933296f, 0.02111687933179302f, 0.023651014781892105f, 0.026138413719924366f, 0.028504805554540335f, 0.030673712650812642f, 0.032570469110863566f, 0.0341264481423066f, 0.035283167975905166f, 0.03599593524940756f, 0.03623670984128611f, 0.035283167975905166f, 0.0341264481423066f, 0.032570469110863566f, 0.030673712650812642f, 0.028504805554540335f, 0.026138413719924366f, 0.023651014781892105f, 0.02111687933179302f, 0.018604547169933296f}, 
                    {0.020020151787989493f, 0.022723645227677437f, 0.025450600949787093f, 0.0281272640172531f, 0.030673712650812642f, 0.03300765009550321f, 0.0350487291869867f, 0.036723101376961914f, 0.0379678350667045f, 0.03873483621296677f, 0.03899393114454834f, 0.0379678350667045f, 0.036723101376961914f, 0.0350487291869867f, 0.03300765009550321f, 0.030673712650812642f, 0.0281272640172531f, 0.025450600949787093f, 0.022723645227677437f, 0.020020151787989493f}, 
                    {0.02125812883587265f, 0.024128796973478923f, 0.02702437822002595f, 0.029866556887849564f, 0.032570469110863566f, 0.0350487291869867f, 0.03721602156071347f, 0.03899393114454834f, 0.040315634867033275f, 0.04113006471537673f, 0.041405181182754706f, 0.040315634867033275f, 0.03899393114454834f, 0.03721602156071347f, 0.0350487291869867f, 0.032570469110863566f, 0.029866556887849564f, 0.02702437822002595f, 0.024128796973478923f, 0.02125812883587265f}, 
                    {0.02227368690486281f, 0.02528149459097062f, 0.02831540555233173f, 0.031293362749954694f, 0.0341264481423066f, 0.036723101376961914f, 0.03899393114454834f, 0.04085677625764538f, 0.042241621326696954f, 0.043094958682399f, 0.04338321820439457f, 0.042241621326696954f, 0.04085677625764538f, 0.03899393114454834f, 0.036723101376961914f, 0.0341264481423066f, 0.031293362749954694f, 0.02831540555233173f, 0.02528149459097062f, 0.02227368690486281f}, 
                    {0.023028656050868932f, 0.026138413719924366f, 0.029275159437710887f, 0.03235405483258585f, 0.035283167975905166f, 0.0379678350667045f, 0.040315634867033275f, 0.042241621326696954f, 0.04367340587656273f, 0.04455566719880168f, 0.044853697310016656f, 0.04367340587656273f, 0.042241621326696954f, 0.040315634867033275f, 0.0379678350667045f, 0.035283167975905166f, 0.03235405483258585f, 0.029275159437710887f, 0.026138413719924366f, 0.023028656050868932f}, 
                    {0.023493865762111734f, 0.026666444703240564f, 0.029866556887849564f, 0.03300765009550321f, 0.03599593524940756f, 0.03873483621296677f, 0.04113006471537673f, 0.043094958682399f, 0.04455566719880168f, 0.045455751382003626f, 0.045759802100833476f, 0.04455566719880168f, 0.043094958682399f, 0.04113006471537673f, 0.03873483621296677f, 0.03599593524940756f, 0.03300765009550321f, 0.029866556887849564f, 0.026666444703240564f, 0.023493865762111734f}, 
                    {0.023651014781892105f, 0.026844814907981403f, 0.0300663324457192f, 0.03322843623220306f, 0.03623670984128611f, 0.03899393114454834f, 0.041405181182754706f, 0.04338321820439457f, 0.044853697310016656f, 0.045759802100833476f, 0.046065886596178066f, 0.045759802100833476f, 0.044853697310016656f, 0.04338321820439457f, 0.041405181182754706f, 0.03899393114454834f, 0.03623670984128611f, 0.03322843623220306f, 0.0300663324457192f, 0.026844814907981403f}, 
                    {0.023028656050868932f, 0.026138413719924366f, 0.029275159437710887f, 0.03235405483258585f, 0.035283167975905166f, 0.0379678350667045f, 0.040315634867033275f, 0.042241621326696954f, 0.04367340587656273f, 0.04455566719880168f, 0.045759802100833476f, 0.04367340587656273f, 0.042241621326696954f, 0.040315634867033275f, 0.0379678350667045f, 0.035283167975905166f, 0.03235405483258585f, 0.029275159437710887f, 0.026138413719924366f, 0.023028656050868932f}, 
                    {0.02227368690486281f, 0.02528149459097062f, 0.02831540555233173f, 0.031293362749954694f, 0.0341264481423066f, 0.036723101376961914f, 0.03899393114454834f, 0.04085677625764538f, 0.042241621326696954f, 0.043094958682399f, 0.044853697310016656f, 0.042241621326696954f, 0.04085677625764538f, 0.03899393114454834f, 0.036723101376961914f, 0.0341264481423066f, 0.031293362749954694f, 0.02831540555233173f, 0.02528149459097062f, 0.02227368690486281f}, 
                    {0.02125812883587265f, 0.024128796973478923f, 0.02702437822002595f, 0.029866556887849564f, 0.032570469110863566f, 0.0350487291869867f, 0.03721602156071347f, 0.03899393114454834f, 0.040315634867033275f, 0.04113006471537673f, 0.04338321820439457f, 0.040315634867033275f, 0.03899393114454834f, 0.03721602156071347f, 0.0350487291869867f, 0.032570469110863566f, 0.029866556887849564f, 0.02702437822002595f, 0.024128796973478923f, 0.02125812883587265f}, 
                    {0.020020151787989493f, 0.022723645227677437f, 0.025450600949787093f, 0.0281272640172531f, 0.030673712650812642f, 0.03300765009550321f, 0.0350487291869867f, 0.036723101376961914f, 0.0379678350667045f, 0.03873483621296677f, 0.041405181182754706f, 0.0379678350667045f, 0.036723101376961914f, 0.0350487291869867f, 0.03300765009550321f, 0.030673712650812642f, 0.0281272640172531f, 0.025450600949787093f, 0.022723645227677437f, 0.020020151787989493f}, 
                    {0.018604547169933296f, 0.02111687933179302f, 0.023651014781892105f, 0.026138413719924366f, 0.028504805554540335f, 0.030673712650812642f, 0.032570469110863566f, 0.0341264481423066f, 0.035283167975905166f, 0.03599593524940756f, 0.03899393114454834f, 0.035283167975905166f, 0.0341264481423066f, 0.032570469110863566f, 0.030673712650812642f, 0.028504805554540335f, 0.026138413719924366f, 0.023651014781892105f, 0.02111687933179302f, 0.018604547169933296f}, 
                    {0.017060048000296084f, 0.01936381314343707f, 0.02168757152481869f, 0.023968473332915102f, 0.026138413719924366f, 0.0281272640172531f, 0.029866556887849564f, 0.031293362749954694f, 0.03235405483258585f, 0.03300765009550321f, 0.03623670984128611f, 0.03235405483258585f, 0.031293362749954694f, 0.029866556887849564f, 0.0281272640172531f, 0.026138413719924366f, 0.023968473332915102f, 0.02168757152481869f, 0.01936381314343707f, 0.017060048000296084f}, 
                    {0.01543656978415747f, 0.017521102688038404f, 0.019623726221987173f, 0.02168757152481869f, 0.023651014781892105f, 0.025450600949787093f, 0.02702437822002595f, 0.02831540555233173f, 0.029275159437710887f, 0.029866556887849564f, 0.03322843623220306f, 0.029275159437710887f, 0.02831540555233173f, 0.02702437822002595f, 0.025450600949787093f, 0.023651014781892105f, 0.02168757152481869f, 0.019623726221987173f, 0.017521102688038404f, 0.01543656978415747f}, 
                    {0.013782587531019143f, 0.015643768972929527f, 0.017521102688038404f, 0.01936381314343707f, 0.02111687933179302f, 0.022723645227677437f, 0.024128796973478923f, 0.02528149459097062f, 0.026138413719924366f, 0.026666444703240564f, 0.0300663324457192f, 0.026138413719924366f, 0.02528149459097062f, 0.024128796973478923f, 0.022723645227677437f, 0.02111687933179302f, 0.01936381314343707f, 0.017521102688038404f, 0.015643768972929527f, 0.013782587531019143f}, 
                    {0.012142835871516428f, 0.013782587531019143f, 0.01543656978415747f, 0.017060048000296084f, 0.018604547169933296f, 0.020020151787989493f, 0.02125812883587265f, 0.02227368690486281f, 0.023028656050868932f, 0.023493865762111734f, 0.026844814907981403f, 0.023028656050868932f, 0.02227368690486281f, 0.02125812883587265f, 0.020020151787989493f, 0.018604547169933296f, 0.017060048000296084f, 0.01543656978415747f, 0.013782587531019143f, 0.012142835871516428f}
};

}