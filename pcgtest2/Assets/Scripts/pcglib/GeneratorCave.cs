﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;

public class GeneratorCave : MonoBehaviour {
    GameManager gm;

    public enum CaveMode { lakes, walls, both };
    public CaveMode caveMode = CaveMode.walls;

    public float caveThreshold = 0.25f; //TODO dynamically determine from average of all values
    private Texture2D noiseTexture;
    private Texture2D cavemap;

    public Generator generator;

    // producing cave according to terrain sketch
    //private int sketchTileWidth = 10;
    //private int sketchTileHeight = 10;
    public Color keyColor = Color.green;


    public GameObject go_startPoint;
    public GameObject go_finishPoint;
    public GameObject go_bossEnemy;
    public GameObject go_chest;
    public GameObject go_enemy;
    public GameObject go_finding;
    public GameObject go_key;

	// Use this for initialization
    void Start()
    {
        if (go_startPoint == null || go_finishPoint == null || go_bossEnemy == null || go_chest == null || go_enemy == null || go_finding == null || go_key == null)
        {
            throw new System.ArgumentNullException("GeneratorCave: GameObjects for instanziation not initialized");
        }
        gm = FindObjectOfType<GameManager>();
        gm.GenerateMission();
        gm.GenerateMissionPositions(generator.noiseWidth, generator.noiseHeight);

        noiseTexture = new Texture2D(generator.noiseWidth, generator.noiseHeight, TextureFormat.RGB24, false);
        cavemap = new Texture2D(noiseTexture.width, noiseTexture.height, TextureFormat.RGB24, false);

        Generate();
    }

    /// <summary>
    /// starts generation process
    /// </summary>
    private void Generate()
    {
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();
        GenerateMesh();
    }

    /// <summary>
    /// generates mesh based on noise texture
    /// </summary>
    private void GenerateMesh()
    {        
        noiseTexture = generator.GenerateNoiseTexture();
        cavemap = Generator.CreateWideSketch(gm.Sketch, Color.green);

#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("noisetexture_afterwidesketch.png", cavemap.EncodeToPNG());
#endif


        cavemap = Generator.AddTexturesKey(noiseTexture, cavemap, keyColor);


#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("noisetexture_afterkeying.png", cavemap.EncodeToPNG());
#endif


        cavemap = GenerateCaveMap(cavemap);

#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("noisetexture_aftercavemap.png", cavemap.EncodeToPNG());
#endif


        generator.GenerateGeometryFromHeightmap(cavemap);

        // to create pretty picture for thesis ;)
#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("noisetexture_aftercave_withoutsketch.png", GenerateCaveMap(noiseTexture).EncodeToPNG());
#endif

        gameObject.AddComponent<MeshCollider>();

        // place objects in scene, depending on coordinates in sketchLineSegments
        List<MissionNode> nodes = gm.Mg.GetNodes();
        float ratioX = generator.meshSize.x / (float)noiseTexture.width;
        float ratioZ = generator.meshSize.z / (float)noiseTexture.height;
        for (int i = 0; i < nodes.Count; i++)
        {
            int normX = (int)(nodes[i].PointX * ratioX);
            int normZ = (int)(nodes[i].PointY * ratioZ);

            float h = 15; // HACK default height when generated with walls
            Vector3 coordinates = new Vector3(normX, h+1, normZ);
            switch (nodes[i].Task)
            {
                case "S":
                    {
                        Instantiate(go_startPoint, coordinates, Quaternion.identity);
                        //gm.MovePlayer(new Vector3(coordinates.x + 2, coordinates.y, coordinates.z + 2));
                        break;
                    }
                case "E":
                    {
                        Instantiate(go_finishPoint, coordinates, Quaternion.identity);
                        break;
                    }
                case "F":
                    {
                        Instantiate(go_finding, coordinates, Quaternion.identity);
                        break;
                    }
                case "Ke":
                    {
                        Instantiate(go_enemy, coordinates, Quaternion.identity);
                        break;
                    }
                case "Kb":
                    {
                        Instantiate(go_bossEnemy, coordinates, Quaternion.identity);
                        break;
                    }
                case "K":
                    {
                        Instantiate(go_key, coordinates, Quaternion.identity);
                        break;
                    }
                case "C":
                    {
                        Instantiate(go_chest, coordinates, Quaternion.identity);
                        break;
                    }
                default: throw new System.ArgumentException("GeneratorCave: unknown Task used in placement of point of interest marker");
            }
        }
    }

    /// <summary>
    /// Reduces noise to black/white texture according to threshold.
    /// Pixel values under threshold are set to white, above to black
    /// </summary>
    /// <param name="noiseTexture">input noise texture</param>
    /// <returns>black/white texture</returns>
    private Texture2D GenerateCaveMap(Texture2D noiseTexture, bool fixedThreshhold = true)
    {
        Texture2D tmpTex = new Texture2D(noiseTexture.width, noiseTexture.height, TextureFormat.RGB24, false);

        int width = noiseTexture.width;
        int height = noiseTexture.height;

        float threshhold;

        if (fixedThreshhold) threshhold = caveThreshold;
        else threshhold = DetermineTreshhold(noiseTexture);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {

                // build that wall
                if (x == 0 || y == 0 || x == width - 1 || y == height - 1)
                {
                    tmpTex.SetPixel(x, y, Color.white);
                    continue;
                }

                float c = noiseTexture.GetPixel(x, y).grayscale;

                //System.IO.File.AppendAllText(debugFilename, c.ToString() + "\n"); 
                if (c < threshhold)
                {
                    switch (caveMode)
                    {
                        case CaveMode.lakes: tmpTex.SetPixel(x, y, Color.black); break;
                        case CaveMode.walls: tmpTex.SetPixel(x, y, Color.white); break;
                        case CaveMode.both: /*CreateLakeWallsInCave(Color.gray, 0, 0); */ throw new UnityException("CaveMode.both not implemented"); // deactivated, time to generate goes from a second to "I got coffee and still isn't finished, I'm gonna kill the process"
                    }
                    //noiseTexture.SetPixel(x, y, Color.red); // wall or lake, not yet processed
                }
                else
                {
                    tmpTex.SetPixel(x, y, Color.gray); // floor
                }
            }
        }

        #if UNITY_EDITOR && !UNITY_WEBPLAYER
            File.WriteAllBytes("noisetexture_aftercave.png", tmpTex.EncodeToPNG());
        #endif

        return tmpTex;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    /// <summary>
    /// callback function that provies GUI elements
    /// </summary>
    void OnGUI()
    {
        if (GUI.Button(new Rect(0, 40, 150, 20), "Generate with Lakes"))
        {
            caveMode = CaveMode.lakes;
            DestroyImmediate(gameObject.GetComponent<MeshCollider>()); // will be added again in GenerateMesh()
            GenerateMesh();            
        }

        if (GUI.Button(new Rect(0, 60, 150, 20), "Generate with Walls"))
        {
            caveMode = CaveMode.walls;
            DestroyImmediate(gameObject.GetComponent<MeshCollider>()); // will be added again in GenerateMesh()
            GenerateMesh();
        }

        gm.MissionDisplay();
    }

    private float DetermineTreshhold(Texture2D noisetexture)
    {
        float threshhold = 0.0f;

        for (int x = 0; x < noisetexture.width; x++)
        {
            for (int y = 0; y < noisetexture.height; y++)
            {
                threshhold += noisetexture.GetPixel(x, y).grayscale;
            }
        }

        return threshhold / (noisetexture.width * noisetexture.height);
    }
}
