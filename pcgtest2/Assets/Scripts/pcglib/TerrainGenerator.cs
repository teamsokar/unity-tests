using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;


public class TerrainGenerator : MonoBehaviour 
{
    public bool randomSeeds = false;

    private static float[,] htmap = null; // declared static so it should be saved when switching a level HACK does not support tiles!
    private bool newHeightMap = true; // flag indicating if a new heightmap has to be generated because there is no existing one

	//Prototypes
	public Texture2D m_splat0, m_splat1, m_splat2, m_splat3;
	public float m_splatTileSize0 = 10.0f;
	public float m_splatTileSize1 = 2.0f;
    public float m_splatTileSize2 = 20.0f;
    public float m_splatTileSize3 = 10.0f;
	public Texture2D m_detail0, m_detail1, m_detail2;
	public GameObject m_tree0, m_tree1, m_tree2;
	//Noise settings. A higher frq will create larger scale details. Each seed value will create a unique look
	public int m_groundSeed = 0;
	public float m_groundFrq = 800.0f;
	public int m_mountainSeed = 1;
	public float  m_mountainFrq = 1200.0f;
	public int m_treeSeed = 2;
	public float  m_treeFrq = 400.0f;
	public int m_detailSeed = 3;
	public float  m_detailFrq = 100.0f;
	//Terrain settings
	public int m_tilesX = 1; //Number of terrain tiles on the x axis
	public int m_tilesZ = 1; //Number of terrain tiles on the z axis
	public float m_pixelMapError = 6.0f; //A lower pixel error will draw terrain at a higher Level of detail but will be slower
	public float m_baseMapDist = 1000.0f; //The distance at which the low res base map will be drawn. Decrease to increase performance
	//Terrain data settings
	public int m_heightMapSize = 513; //Higher number will create more detailed height maps
	public int m_alphaMapSize = 1024; //This is the control map that controls how the splat textures will be blended
	public int m_terrainSize = 2048;
	public int m_terrainHeight = 512;
	public int m_detailMapSize = 512; //Resolutions of detail (Grass) layers
	//Tree settings
	public int m_treeSpacing = 32; //spacing between trees
	public float m_treeDistance = 2000.0f; //The distance at which trees will no longer be drawn
	public float m_treeBillboardDistance = 400.0f; //The distance at which trees meshes will turn into tree billboards
	public float m_treeCrossFadeLength = 20.0f; //As trees turn to billboards there transform is rotated to match the meshes, a higher number will make this transition smoother
	public int m_treeMaximumFullLODCount = 400; //The maximum number of trees that will be drawn in a certain area. 
	//Detail settings
	public DetailRenderMode detailMode;
	public int m_detailObjectDistance = 400; //The distance at which details will no longer be drawn
	public float m_detailObjectDensity = 4.0f; //Creates more dense details within patch
	public int m_detailResolutionPerPatch = 32; //The size of detail patch. A higher number may reduce draw calls as details will be batch in larger patches
	public float m_wavingGrassStrength = 0.4f;
	public float m_wavingGrassAmount = 0.2f;
	public float m_wavingGrassSpeed = 0.4f;
	public Color m_wavingGrassTint = Color.white;
	public Color m_grassHealthyColor = Color.white;
	public Color m_grassDryColor = Color.white;

	//Private
	PerlinNoise m_groundNoise, m_mountainNoise, m_treeNoise, m_detailNoise;
	Terrain[,] m_terrain;
	SplatPrototype[] m_splatPrototypes;
	TreePrototype[] m_treeProtoTypes;
	DetailPrototype[] m_detailProtoTypes;
	Vector2 m_offset;

    private Texture2D sketch;
    private List<Vector2> sketchLineSegments;

    private List<GameObject> interactableObjects;
    // references to prefabs
    public GameObject go_caveEntrance;
    public GameObject go_dungeonEntrance;
    public GameObject go_startPoint;
    public GameObject go_finishPoint;

    private GameManager gm;
    
    void OnGUI()
    {
        if (GUI.Button(new Rect(0, 40, 150, 20), "Generate"))
        {
            GameObject[] gms = GameObject.FindGameObjectsWithTag("Terrain");
            foreach (GameObject gm in gms)
            {
                Destroy(gm);
            }

            Generate();
        }
    }
    
	/// <summary>
	/// Initialization
	/// </summary>
	void Start() 
	{
        gm = FindObjectOfType<GameManager>();
        gm.GenerateMissionPositions(m_heightMapSize, m_heightMapSize); // mandatory!
        Generate();
	}

    /// <summary>
    /// starts generation process
    /// </summary>
    private void Generate()
    {
        // seeds can be static to generate the same landscape again, possible to use with a persitent world? -> save seed

        if (go_caveEntrance == null || go_dungeonEntrance == null || go_startPoint == null || go_finishPoint == null)
        {
            throw new System.ArgumentNullException("TerrainGenerator: Prefabs for points of interest not specified");
        }

        m_tilesX = m_tilesZ = 1; // HACK for export, otherwise takes 2x2 from Prefab

        if(randomSeeds){
            m_groundSeed = UnityEngine.Random.Range(0, 101);
            m_mountainSeed = UnityEngine.Random.Range(0, 101);
            m_treeSeed = UnityEngine.Random.Range(0, 101);
            m_detailSeed = UnityEngine.Random.Range(0, 101);
        }

        m_groundNoise = new PerlinNoise(m_groundSeed);
        m_mountainNoise = new PerlinNoise(m_mountainSeed);
        //m_treeNoise = new PerlinNoise(m_treeSeed);
        //m_detailNoise = new PerlinNoise(m_detailSeed);

        if (!Mathf.IsPowerOfTwo(m_heightMapSize - 1))
        {
            Debug.Log("TerrianGenerator::Start - height map size must be pow2+1 number");
            m_heightMapSize = Mathf.ClosestPowerOfTwo(m_heightMapSize) + 1;
        }

        if (!Mathf.IsPowerOfTwo(m_alphaMapSize))
        {
            Debug.Log("TerrianGenerator::Start - Alpha map size must be pow2 number");
            m_alphaMapSize = Mathf.ClosestPowerOfTwo(m_alphaMapSize);
        }

        if (!Mathf.IsPowerOfTwo(m_detailMapSize))
        {
            Debug.Log("TerrianGenerator::Start - Detail map size must be pow2 number");
            m_detailMapSize = Mathf.ClosestPowerOfTwo(m_detailMapSize);
        }

        if (m_detailResolutionPerPatch < 8)
        {
            Debug.Log("TerrianGenerator::Start - Detail resolution per patch must be >= 8, changing to 8");
            m_detailResolutionPerPatch = 8;
        }

        //check if a heightmap already exists
        if (htmap == null)
        {
            htmap = new float[m_heightMapSize, m_heightMapSize];
            newHeightMap = true;
        }
        else
        {
            newHeightMap = false;
        }

        m_terrain = new Terrain[m_tilesX, m_tilesZ];
        

        //this will center terrain at origin
        m_offset = new Vector2(-m_terrainSize * m_tilesX * 0.5f, -m_terrainSize * m_tilesZ * 0.5f);

        CreateProtoTypes();

        for (int x = 0; x < m_tilesX; x++)
        {
            for (int z = 0; z < m_tilesZ; z++)
            {

                if (newHeightMap)
                {
                    FillHeights(htmap, x, z); 
                }
                
                //OPTIONAL first create all heightmaps to determine global maxmium for splatmaps -> no discrepancies und hard texture edge between tiles
                TerrainData terrainData = new TerrainData();

                terrainData.heightmapResolution = m_heightMapSize;
                terrainData.SetHeights(0, 0, htmap);
                terrainData.size = new Vector3(m_terrainSize, m_terrainHeight, m_terrainSize);
                terrainData.splatPrototypes = m_splatPrototypes;
                terrainData.treePrototypes = m_treeProtoTypes;
                terrainData.detailPrototypes = m_detailProtoTypes;


                //FillAlphaMap(terrainData);
                AssignSplatMap(terrainData);

                m_terrain[x, z] = Terrain.CreateTerrainGameObject(terrainData).GetComponent<Terrain>();
                m_terrain[x, z].transform.position = new Vector3(m_terrainSize * x + m_offset.x, 0, m_terrainSize * z + m_offset.y);
                m_terrain[x, z].heightmapPixelError = m_pixelMapError;
                m_terrain[x, z].basemapDistance = m_baseMapDist;

                //disable this for better frame rate
                m_terrain[x, z].castShadows = false;

                m_terrain[x, z].tag = "Terrain"; // to delete them later

                // place details (bushes, etc) and trees
                //FillTreeInstances(m_terrain[x,z], x, z);
                //FillDetailMap(m_terrain[x,z], x, z);

                // place objects to switch scene, depending on coordinates sketchLineSegments
                List<MissionNode> nodes = GameManager.LandscapeMission.GetNodes();
                float ratio = (float)m_terrainSize / (float)m_heightMapSize;
                for (int i = 0; i < nodes.Count; i++)
                {
                    // translate coordinates from heightmap to terrain
                    int normX = (int) ((nodes[i].PointY * ratio)+m_offset.x); // usage of point.y here
                    int normZ = (int) ((nodes[i].PointX * ratio)+m_offset.y); // and point.x here is correct, because for some reason they are switched around

                    float h = Terrain.activeTerrain.SampleHeight(new Vector3(normX, 0, normZ)); // very costly operation, but works
                                                  //terrainData.GetHeight((int)nodes[i].Point.x, (int)nodes[i].Point.y); // this doesn't because GetHeight uses other parameters for points, which are normalized between 0 and 1 - but this is just a guess, its not officially documented
                    Vector3 coordinates = new Vector3(normX, h+1, normZ);
                    switch (nodes[i].Task)
                    {
                        case "Od":
                            {
                                Instantiate(go_dungeonEntrance, coordinates, Quaternion.identity);
                                gm.LastPositionOnLandscape = new Vector3(coordinates.x + 10, coordinates.y + 2, coordinates.z + 10);
                                break;
                            }
                        case "Oc":
                            {
                                gm.LastPositionOnLandscape = new Vector3(coordinates.x + 10, coordinates.y + 2, coordinates.z + 10);
                                Instantiate(go_caveEntrance, coordinates, Quaternion.identity);
                                break;
                            }
                        case "S":
                            {
                                Instantiate(go_startPoint, coordinates, Quaternion.identity);
                                float h2 = Terrain.activeTerrain.SampleHeight(new Vector3(coordinates.x+2, coordinates.y,coordinates.z+2));
                                //gm.MovePlayer(new Vector3(coordinates.x + 2, h2 + 3, coordinates.z + 2)); // move player to starting coordinates
                                break;
                            }
                        case "E":
                            {
                                Instantiate(go_finishPoint, coordinates, Quaternion.identity);
                                break;
                            }
                        default: throw new System.ArgumentException("Terrain Generator: unkown task to place point of interest marker");
                        
                    }
                }

                /*
                if (!newHeightMap)
                {
                    //gm.MovePlayer(gm.LastPositionOnLandscape); // move player back to position near level transistion
                }
                */

            #if UNITY_EDITOR && !UNITY_WEBPLAYER
                File.WriteAllBytes("noisetexture_terrain.png", Generator.convertHeightmap(htmap).EncodeToPNG());
            #endif
            }
        }

        //Set the neighbours of terrain to remove seams.
        
        for (int x = 0; x < m_tilesX; x++)
        {
            for (int z = 0; z < m_tilesZ; z++)
            {
                Terrain right = null;
                Terrain left = null;
                Terrain bottom = null;
                Terrain top = null;

                if (x > 0) left = m_terrain[(x - 1), z];
                if (x < m_tilesX - 1) right = m_terrain[(x + 1), z];

                if (z > 0) bottom = m_terrain[x, (z - 1)];
                if (z < m_tilesZ - 1) top = m_terrain[x, (z + 1)];

                m_terrain[x, z].SetNeighbors(left, top, right, bottom);

            }
        }
    }
	
    /// <summary>
    /// initialize hardcoded prototypes for textures, details and trees
    /// </summary>
	void CreateProtoTypes()
	{
		//Ive hard coded 2 splat prototypes, 3 tree prototypes and 3 detail prototypes.
		//This is a little inflexible way to do it but it made the code simpler and can easly be modified 
		
		m_splatPrototypes = new SplatPrototype[4];
		
		m_splatPrototypes[0] = new SplatPrototype();
		m_splatPrototypes[0].texture = m_splat0;
		m_splatPrototypes[0].tileSize = new Vector2(m_splatTileSize0, m_splatTileSize0);
		
		m_splatPrototypes[1] = new SplatPrototype();
		m_splatPrototypes[1].texture = m_splat1;
		m_splatPrototypes[1].tileSize = new Vector2(m_splatTileSize1, m_splatTileSize1);

        m_splatPrototypes[2] = new SplatPrototype();
        m_splatPrototypes[2].texture = m_splat2;
        m_splatPrototypes[2].tileSize = new Vector2(m_splatTileSize2, m_splatTileSize2);

        m_splatPrototypes[3] = new SplatPrototype();
        m_splatPrototypes[3].texture = m_splat3;
        m_splatPrototypes[3].tileSize = new Vector2(m_splatTileSize3, m_splatTileSize3);
		
		m_treeProtoTypes = new TreePrototype[3];
		
		m_treeProtoTypes[0] = new TreePrototype();
		m_treeProtoTypes[0].prefab = m_tree0;
		
		m_treeProtoTypes[1] = new TreePrototype();
		m_treeProtoTypes[1].prefab = m_tree1;
		
		m_treeProtoTypes[2] = new TreePrototype();
		m_treeProtoTypes[2].prefab = m_tree2;
		
		m_detailProtoTypes = new DetailPrototype[3];

		m_detailProtoTypes[0] = new DetailPrototype();
		m_detailProtoTypes[0].prototypeTexture = m_detail0;
		m_detailProtoTypes[0].renderMode = detailMode;
		m_detailProtoTypes[0].healthyColor = m_grassHealthyColor;
		m_detailProtoTypes[0].dryColor = m_grassDryColor;
		
		m_detailProtoTypes[1] = new DetailPrototype();
		m_detailProtoTypes[1].prototypeTexture = m_detail1;
		m_detailProtoTypes[1].renderMode = detailMode;
		m_detailProtoTypes[1].healthyColor = m_grassHealthyColor;
		m_detailProtoTypes[1].dryColor = m_grassDryColor;
		
		m_detailProtoTypes[2] = new DetailPrototype();
		m_detailProtoTypes[2].prototypeTexture = m_detail2;
		m_detailProtoTypes[2].renderMode = detailMode;
		m_detailProtoTypes[2].healthyColor = m_grassHealthyColor;
		m_detailProtoTypes[2].dryColor = m_grassDryColor;

		
	}
	
    /// <summary>
    /// Create terrain heightmap for current tile
    /// </summary>
    /// <param name="htmap">Heightmap</param>
    /// <param name="tileX">current tile in x direction</param>
    /// <param name="tileZ">current tile in z direction</param>
	void FillHeights(float[,] htmap, int tileX, int tileZ)
	{
		float ratio = (float)m_terrainSize/(float)m_heightMapSize;

        float[,] htmap_plains = new float[m_heightMapSize, m_heightMapSize];
        float[,] htmap_mountains = new float[m_heightMapSize, m_heightMapSize];

        // generate mountains
        for (int x = 0; x < m_heightMapSize; x++)
		{
			for(int z = 0; z < m_heightMapSize; z++)
			{
                //scales noise onto complete landscape
				float worldPosX = (x+tileX*(m_heightMapSize-1))*ratio;
				float worldPosZ = (z+tileZ*(m_heightMapSize-1))*ratio;

                htmap_mountains[z, x] = Mathf.Max(0.0f, m_mountainNoise.FractalNoise2D(worldPosX, worldPosZ, 6, m_mountainFrq, 0.8f));
            }
        }


#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("noisetexture_terrain_mountains.png", Generator.convertHeightmap(htmap_mountains).EncodeToPNG());
#endif        

        // generate plains
        for (int x = 0; x < m_heightMapSize; x++)
        {
            for (int z = 0; z < m_heightMapSize; z++)
            {
                //scales noise onto complete landscape
                float worldPosX = (x + tileX * (m_heightMapSize - 1)) * ratio;
                float worldPosZ = (z + tileZ * (m_heightMapSize - 1)) * ratio;

                htmap_plains[z, x] = m_groundNoise.FractalNoise2D(worldPosX, worldPosZ, 4, m_groundFrq, 0.1f) + 0.1f;
            }
        }
#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("noisetexture_terrain_plain.png", Generator.convertHeightmap(htmap_plains).EncodeToPNG());
#endif

        // OPTIONAL scale sketch onto terrain with multiple tiles
        //float[,] sketchpart = new float[sketch.GetLength(0) / m_tilesX, sketch.GetLength(1) / m_tilesZ];
        //float[,] weights = Generator.GenerateHeightmapWeightsCoarse(sketch, Color.green, sketchLineSegments); // weight of plains, negate value for mountains
        float[,] weights = Generator.GenerateHeightmapWeightsSmooth(gm.Sketch, Color.green);


#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("noisetexture_terrain_weights.png", Generator.convertHeightmap(weights).EncodeToPNG());
#endif

        // add mountains and plain
        for (int x = 0; x < m_heightMapSize; x++)
        {
            for (int z = 0; z < m_heightMapSize; z++)
            {
                htmap[z, x] = (htmap_plains[z, x] /* * weights[z,x]*/ + htmap_mountains[z, x]*(1.0f-weights[z,x]));
            }
        }

#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("noisetexture_terrain_complete.png", Generator.convertHeightmap(htmap).EncodeToPNG());
#endif


    }
	
    /// <summary>
    /// Create alphamaps for splattextures of current terrain tile
    /// Original example, only supports two splat textures
    /// </summary>
    /// <param name="terrainData">terrainData of current tile</param>
	void FillAlphaMap(TerrainData terrainData) 
	{
        float[,,] map  = new float[m_alphaMapSize, m_alphaMapSize, 3];
		
		//Random.seed = 0;
        
        for(int x = 0; x < m_alphaMapSize; x++) 
		{
            for (int z = 0; z < m_alphaMapSize; z++) 
			{
                // Get the normalized terrain coordinate that
                // corresponds to the the point.
                float normX = x * 1.0f / (m_alphaMapSize - 1);
                float normZ = z * 1.0f / (m_alphaMapSize - 1);
                
                // Get the steepness value at the normalized coordinate.
                float angle = terrainData.GetSteepness(normX, normZ);
                
                // Steepness is given as an angle, 0..90 degrees. Divide
                // by 90 to get an alpha blending value in the range 0..1.
                float frac = angle / 90.0f;
                //map[z, x, 0] = frac * 0.75f;
                //map[z, x, 1] = frac;
                //map[z, x, 2] = 1.0f - frac;
                map[z, x, 0] = 1 - frac;
                map[z, x, 1] = frac;
				
            }
        }
        
		terrainData.alphamapResolution = m_alphaMapSize;
        terrainData.SetAlphamaps(0, 0, map);
    }

    /// <summary>
    /// Create alphamaps for splattextures of current tile
    /// From: https://alastaira.wordpress.com/2013/11/14/procedural-terrain-splatmapping/
    /// supports multiple spalt textures
    /// </summary>
    /// <param name="terrainData">terrainData for current tile</param>
    void AssignSplatMap(TerrainData terrainData)
    {
        float[, ,] splatmapData = new float[terrainData.alphamapWidth, terrainData.alphamapHeight, terrainData.alphamapLayers];

        /// x = min; y = max
        //Vector2 heightmapMinMaxHeight = getHeightmapMinMaxHeight(terrainData.GetHeights(0, 0, terrainData.heightmapWidth, terrainData.heightmapHeight));
        //Vector2 heightmapMinMaxHeight = getHeightmapMinMaxHeight(terrainData);
        //float heightmapMaxHeight = getHeightmapMaxHeight(terrainData.GetHeights(0,0,terrainData.heightmapWidth,terrainData.heightmapHeight));
        float heightmapMaxHeight = getHeightmapMaxHeight(terrainData);


        for (int x = 0; x < terrainData.alphamapWidth; x++)
        {
            for (int y = 0; y < terrainData.alphamapHeight; y++)
            {
                float normX = (float)x / (float)terrainData.alphamapWidth;
                float normY = (float)y / (float)terrainData.alphamapHeight;

                // interchange x and y - for no apparent reason, but then it works
                float height = terrainData.GetHeight(Mathf.RoundToInt(normY * terrainData.heightmapWidth), Mathf.RoundToInt(normX * terrainData.heightmapHeight));

                //Vector3 normal = terrainData.GetInterpolatedNormal(normX, normY);
                //float steepness = terrainData.GetSteepness(normX, normY);

                float [] splatWeights = new float[terrainData.alphamapLayers];

                //hardcoded rules
				/*
                splatWeights[0] = Mathf.Clamp01(terrainData.heightmapHeight - height);
                splatWeights[1] = 1.0f - Mathf.Clamp01(steepness * steepness / (terrainData.heightmapHeight / 5.0f));
                splatWeights[2] = Mathf.Clamp01(steepness * steepness / (terrainData.heightmapHeight / 5.0f));
                //splatWeights[2] = height * Mathf.Clamp01(normal.z);
				*/
				
				 //EASY DEBUG
                //splatWeights[0] = height > 200.0f ? 0.0f : 1.0f;
                //splatWeights[1] = height > 200.0f ? 1.0f : 0.0f;
                //splatWeights[2] = 0.0f;

                //DEMO
                //uses percentage to max height as weight
                //should actually use real existing max height as kind of a clamp

                //float heightPerc = (terrainData.heightmapHeight - height) / terrainData.heightmapHeight;
                //float heightPerc = (heightmapMinMaxHeight.y - height) / heightmapMinMaxHeight.y;
                //float heightPerc = 1.0f - (height / heightmapMinMaxHeight.y);
                float heightPerc = 1.0f - (height / heightmapMaxHeight);

                if (heightPerc < 0.1f)
                {
                    splatWeights[0] = 0.0f;
                    splatWeights[1] = 0.0f;
                    splatWeights[2] = 1.0f;
                    splatWeights[3] = 0.0f;
                }
                else if (heightPerc < 0.3f)
                {
                    splatWeights[0] = 0.0f;
                    splatWeights[1] = 1.0f;
                    splatWeights[2] = 0.0f;
                    splatWeights[3] = 0.0f;
                }
                else if (heightPerc < 0.6f)
                {
                    splatWeights[0] = 0.4f;
                    splatWeights[1] = 0.6f;
                    splatWeights[2] = 0.0f;
                    splatWeights[3] = 0.0f;
                }
                else if(heightPerc < 0.8f)
                {
                    splatWeights[0] = 0.7f;
                    splatWeights[1] = 0.3f;
                    splatWeights[2] = 0.0f;
                    splatWeights[3] = 0.0f;
                }
                else
                {
                    splatWeights[0] = 1.0f;
                    splatWeights[1] = 0.0f;
                    splatWeights[2] = 0.0f;
                    splatWeights[3] = 0.0f;
                }

                /*
                splatWeights[0] = 1.0f - heightPerc;
                splatWeights[1] = heightPerc;
                splatWeights[2] = 0.0f;
                splatWeights[3] = 0.0f;
                 */

                //terrainData.heightmapHeight;

				

                float z = splatWeights.Sum();

                for (int i = 0; i < terrainData.alphamapLayers; i++)
                {
                    splatWeights[i] /= z;

                    splatmapData[x, y, i] = splatWeights[i];
                }
            }
        }

        //terrainData.alphamapResolution = m_alphaMapSize;
        terrainData.SetAlphamaps(0, 0, splatmapData);
    }

    /// <summary>
    /// Get maximum height in heightmap
    /// </summary>
    /// <param name="heights">heights of current terrain segment</param>
    /// <returns>maximum height</returns>
    private float getHeightmapMaxHeight(float[,] heights)
    {
        float max = -1;
        for (int x = 0; x < heights.GetLength(0); x++)
        {
            for (int y = 0; y < heights.GetLength(1); y++)
            {
                float height = heights[x, y];
                if (x == 0 && y == 0)
                {
                    max = height;
                    continue;
                }

                if (height > max) max = height;
            }
        }
        return max;
    }

    /// <summary>
    /// Get maximum height in heightmap
    /// </summary>
    /// <param name="terrainData">current terrain segment</param>
    /// <returns>maximum height</returns>
    private float getHeightmapMaxHeight(TerrainData terrainData)
    {
        float max = -1;
        for (int x = 0; x < terrainData.heightmapWidth; x++)
        {
            for (int y = 0; y < terrainData.heightmapHeight; y++)
            {
                float height = terrainData.GetHeight(x, y);
                if (x == 0 && y == 0)
                {
                    max = height;
                    continue;
                }

                if (height > max) max = height;
            }
        }
        return max;
    }

    /// <summary>
    /// Get minimum and maximum heights
    /// </summary>
    /// <param name="terrainData">current terrain data</param>
    /// <returns>Vector2: x is minimum, y is maximum</returns>
    private Vector2 getHeightmapMinMaxHeight(TerrainData terrainData)
    {
        Vector2 minMax = new Vector2();
        for (int x = 0; x < terrainData.heightmapWidth; x++)
        {
            for (int y = 0; y < terrainData.heightmapHeight; y++)
            {
                float height = terrainData.GetHeight(x, y);

                if (x == 0 && y == 0)
                {
                    minMax.x = height;
                    minMax.y = height;
                    continue;
                }

                // x is minimum, y i maximum
                if (height < minMax.x) minMax.x = height;
                else if (height > minMax.y) minMax.y = height;
            }
        }

        return minMax;
    }

    /// <summary>
    /// Get minimum and maximum heights
    /// </summary>
    /// <param name="heights">2d float array with all heights of heightmap</param>
    /// <returns>Vector2: x is minimum, y is maximum</returns>
    private Vector2 getHeightmapMinMaxHeight(float[,] heights)
    {
        Vector2 minMax = new Vector2();
        for (int x = 0; x < heights.GetLength(0); x++)
        {
            for (int y = 0; y < heights.GetLength(1); y++)
            {
                float height = heights[x, y];

                if (x == 0 && y == 0)
                {
                    minMax.x = height;
                    minMax.y = height;
                    continue;
                }

                // x is minimum, y i maximum
                if (height < minMax.x) minMax.x = height;
                else if (height > minMax.y) minMax.y = height;
            }
        }
        return minMax;
    }
	
    /// <summary>
    /// place trees
    /// </summary>
    /// <param name="terrain"></param>
    /// <param name="tileX"></param>
    /// <param name="tileZ"></param>
	void FillTreeInstances(Terrain terrain, int tileX, int tileZ)
	{
		Random.seed = 0;
	
		for(int x = 0; x < m_terrainSize; x += m_treeSpacing) 
		{
            for (int z = 0; z < m_terrainSize; z += m_treeSpacing) 
			{
				
				float unit = 1.0f / (m_terrainSize - 1);
				
				float offsetX = Random.value * unit * m_treeSpacing;
				float offsetZ = Random.value * unit * m_treeSpacing;
				
                float normX = x * unit + offsetX;
                float normZ = z * unit + offsetZ;
                
                // Get the steepness value at the normalized coordinate.
                float angle = terrain.terrainData.GetSteepness(normX, normZ);
                
                // Steepness is given as an angle, 0..90 degrees. Divide
                // by 90 to get an alpha blending value in the range 0..1.
                float frac = angle / 90.0f;
				
				if(frac < 0.5f) //make sure tree are not on steep slopes
				{
					float worldPosX = x+tileX*(m_terrainSize-1);
					float worldPosZ = z+tileZ*(m_terrainSize-1);
					
					float noise = m_treeNoise.FractalNoise2D(worldPosX, worldPosZ, 3, m_treeFrq, 1.0f);
					float ht = terrain.terrainData.GetInterpolatedHeight(normX, normZ);
					
					if(noise > 0.0f && ht < m_terrainHeight*0.4f)
					{
				
						TreeInstance temp = new TreeInstance();
						temp.position = new Vector3(normX,ht,normZ);
						temp.prototypeIndex = Random.Range(0, 3);
						temp.widthScale = 1;
						temp.heightScale = 1;
						temp.color = Color.white;
						temp.lightmapColor = Color.white;
						
						terrain.AddTreeInstance(temp);
					}
				}
				
			}
		}
		
		terrain.treeDistance = m_treeDistance;
		terrain.treeBillboardDistance = m_treeBillboardDistance;
		terrain.treeCrossFadeLength = m_treeCrossFadeLength;
		terrain.treeMaximumFullLODCount = m_treeMaximumFullLODCount;
		
	}
	
    /// <summary>
    /// place bushes and other small objects
    /// </summary>
    /// <param name="terrain"></param>
    /// <param name="tileX"></param>
    /// <param name="tileZ"></param>
	void FillDetailMap(Terrain terrain, int tileX, int tileZ)
	{
		//each layer is drawn separately so if you have a lot of layers your draw calls will increase 
		int[,] detailMap0 = new int[m_detailMapSize,m_detailMapSize];
		int[,] detailMap1 = new int[m_detailMapSize,m_detailMapSize];
		int[,] detailMap2 = new int[m_detailMapSize,m_detailMapSize];
		
		float ratio = (float)m_terrainSize/(float)m_detailMapSize;
		
		Random.seed = 0;
	
		for(int x = 0; x < m_detailMapSize; x++) 
		{
            for (int z = 0; z < m_detailMapSize; z++) 
			{
				detailMap0[z,x] = 0;
				detailMap1[z,x] = 0;
				detailMap2[z,x] = 0;
					
				float unit = 1.0f / (m_detailMapSize - 1);

                float normX = x * unit;
                float normZ = z * unit;
                
                // Get the steepness value at the normalized coordinate.
                float angle = terrain.terrainData.GetSteepness(normX, normZ);
                
                // Steepness is given as an angle, 0..90 degrees. Divide
                // by 90 to get an alpha blending value in the range 0..1.
                float frac = angle / 90.0f;
				
				if(frac < 0.5f)
				{
					float worldPosX = (x+tileX*(m_detailMapSize-1))*ratio;
					float worldPosZ = (z+tileZ*(m_detailMapSize-1))*ratio;
					
					float noise = m_detailNoise.FractalNoise2D(worldPosX, worldPosZ, 3, m_detailFrq, 1.0f);
					
					if(noise > 0.0f) 
					{
						float rnd = Random.value;
						//Randomly select what layer to use
						if(rnd < 0.33f)
							detailMap0[z,x] = 1;
						else if(rnd < 0.66f)
							detailMap1[z,x] = 1;
						else
							detailMap2[z,x] = 1;
					}
				}
				
			}
		}
		
		terrain.terrainData.wavingGrassStrength = m_wavingGrassStrength;
		terrain.terrainData.wavingGrassAmount = m_wavingGrassAmount;
		terrain.terrainData.wavingGrassSpeed = m_wavingGrassSpeed;
		terrain.terrainData.wavingGrassTint = m_wavingGrassTint;
		terrain.detailObjectDensity = m_detailObjectDensity;
		terrain.detailObjectDistance = m_detailObjectDistance;
		terrain.terrainData.SetDetailResolution(m_detailMapSize, m_detailResolutionPerPatch);
		
		terrain.terrainData.SetDetailLayer(0,0,0,detailMap0);
		terrain.terrainData.SetDetailLayer(0,0,1,detailMap1);
		terrain.terrainData.SetDetailLayer(0,0,2,detailMap2);
		
	}
	

}


