﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System;

/// <summary>
/// Class to generate dungeons
/// </summary>
public class GeneratorDungeon : MonoBehaviour {

    Texture2D dungeonMap;
    public Generator generator;
    GameManager gm;

    public int minRooms = 3;
    public int maxRooms = 7;

    public int CorridorWidth = 3;

    public bool debugRooms = false;

    // Objects 
    public UnityEngine.Object go_startPoint;
    public UnityEngine.Object go_finishPoint;
    public UnityEngine.Object go_finding;
    public UnityEngine.Object go_enemy;
    public UnityEngine.Object go_bossEnemy;
    public UnityEngine.Object go_key;
    public UnityEngine.Object go_chest;

    /// <summary>
    /// Unity default initialization function
    /// </summary>
	void Start () {
        if (go_startPoint == null || go_finishPoint == null || go_bossEnemy == null || go_chest == null || go_enemy == null || go_finding == null || go_key == null)
        {
            throw new System.ArgumentNullException("GeneratorDungeon: GameObjects for instanziation not initialized");
        }

        gm = FindObjectOfType<GameManager>();
        gm.GenerateMission();
        
        gm.GenerateMissionPositions(generator.noiseWidth, generator.noiseHeight);

        // HACK increase dimenions to prevent Rooms being sometimes placed out of bounds. It should not happen, but it does for unknown reasons
        dungeonMap = new Texture2D(generator.noiseWidth, generator.noiseHeight, TextureFormat.RGB24, false);

        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();

        Generate();

        gameObject.AddComponent<MeshCollider>();
	}

    /// <summary>
    /// Default Unity function that is called to draw a graphical user interface
    /// </summary>
    void OnGUI()
    {
        if (GUI.Button(new Rect(0, 40, 150, 20), "Generate"))
        {
            GameObject[] go = GameObject.FindGameObjectsWithTag("Objective");
            for (int i = 0; i < go.Length; i++)
            {
                Destroy(go[i]);
            }

            DestroyImmediate(gameObject.GetComponent<MeshCollider>());
            gm.GenerateMission();
            gm.GenerateMissionPositions(generator.noiseWidth, generator.noiseHeight);
            //dungeonMap = new Texture2D(generator.noiseWidth, generator.noiseHeight, TextureFormat.RGB24, false);
            Generator.clearTexture(dungeonMap, Color.white);
            Generate();            
            gameObject.AddComponent<MeshCollider>();
            
            
        }

        gm.MissionDisplay();
    }

    /// <summary>
    /// Default function to start generation process.
    /// </summary>
    private void Generate()
    {
        List<Room> roomList = new List<Room>();

        // HACK when a room is out of bounds just generate a new one
        bool validRooms = false;
        while (!validRooms)
        {
            if (!debugRooms)
            {
                roomList = GenerateDungeonRooms(dungeonMap.width, dungeonMap.height, gm.Mg.GetNodes().Count);
            }
            else
            {
                roomList = debugRoomList();
            }

            validRooms = GenerateDungeonMap(roomList, dungeonMap); 
        }
        
        Room.CalculateRNG(roomList);
        debugDrawRoomsAndNodes(roomList);
        GenerateCorridor(roomList, dungeonMap, CorridorWidth);

        generator.GenerateGeometryFromHeightmap(dungeonMap);

#if UNITY_EDITOR && !UNITY_WEBPLAYER
        DebugSaveDungeon(roomList, dungeonMap, false);
#endif

        // place objects in scene, depending on coordinates in sketchLineSegments
        float ratioX = generator.meshSize.x / (float)dungeonMap.width;
        float ratioZ = generator.meshSize.z / (float)dungeonMap.height;
        for (int i = 0; i < roomList.Count; i++)
        {
            int normX = (int)(roomList[i].CenterX * ratioX);
            int normZ = (int)(roomList[i].CenterY * ratioZ);

            float h = 0; // HACK default height
            Vector3 coordinates = new Vector3(normX, h + 1, normZ);
            switch (gm.Mg.GetNodes()[i].Task)
            {
                case "S":
                    {
                        Instantiate(go_startPoint, coordinates, Quaternion.identity);
                        //gm.MovePlayer(new Vector3(coordinates.x + 2, coordinates.y, coordinates.z + 2)); // set player to start coordinates, not exactly to prevent instant transition to landscapepg
                        break;
                    }
                case "E":
                    {
                        Instantiate(go_finishPoint, coordinates, Quaternion.identity);
                        break;
                    }
                case "F":
                    {
                        Instantiate(go_finding, coordinates, Quaternion.identity);
                        break;
                    }
                case "Ke":
                    {
                        Instantiate(go_enemy, coordinates, Quaternion.identity);
                        break;
                    }
                case "Kb":
                    {
                        Instantiate(go_bossEnemy, coordinates, Quaternion.identity);
                        break;
                    }
                case "K":
                    {
                        Instantiate(go_key, coordinates, Quaternion.identity);
                        break;
                    }
                case "C":
                    {
                        Instantiate(go_chest, coordinates, Quaternion.identity);
                        break;
                    }
                default: throw new System.ArgumentException("GeneratorCave: unknown Task used in placement of point of interest marker");
            }
        }
    }
    
    /// <summary>
    /// Debug Funtions: writes list of rooms and dungeon map to hdd
    /// </summary>
    /// <param name="roomList">list of rooms</param>
    /// <param name="dungeonMap">dungeon map</param>
    private void DebugSaveDungeon(List<Room> roomList, Texture2D dungeonMap, bool faulty = false, bool formatedOutput = false)
    {
        #if UNITY_EDITOR && !UNITY_WEBPLAYER
        if (!System.IO.Directory.Exists("dungeons"))
            System.IO.Directory.CreateDirectory("dungeons");

        DateTime d = DateTime.UtcNow.AddHours(1.0f); // GMT+1 = Berlin Time Zone
        String filename = "dungeons/" + d.Year + "-" + d.Month + "-" + d.Day + "_" + d.Hour + "-" + d.Minute + "-" + d.Second;

        if (faulty)
        {
            filename += "_faulty";
        }
        
        StreamWriter sw = File.CreateText(filename + ".txt");
        for (int i = 0; i < roomList.Count; i++)
        {
            sw.WriteLine("Room: " + i);

            if (formatedOutput)
            {
                sw.WriteLine("LowerLeft: " + roomList[i].LowerLeftX + " " + roomList[i].LowerLeftY);
                sw.WriteLine("UpperRight: " + roomList[i].UpperRightX + " " + roomList[i].UpperRightY);
                sw.WriteLine("Center: " + roomList[i].CenterY + " " + roomList[i].CenterY);
                sw.WriteLine();
            }
            else
            {
                sw.Write(roomList[i].LowerLeftX + "," + roomList[i].LowerLeftY + " / ");
                sw.Write(roomList[i].UpperRightX + "," + roomList[i].UpperRightY + " / ");
                sw.Write(roomList[i].CenterX + "," + roomList[i].CenterY + "\n");
            }
            
            sw.WriteLine("Neighours:");
            
            for (int j = 0; j < roomList[i].Neighbours.Count; j++)
            {
                Room r = (Room) roomList[i].Neighbours[j];
                sw.Write(r.LowerLeftX + "," + r.LowerLeftY + " / ");
                sw.Write(r.UpperRightX + "," + r.UpperRightY + " / ");
                sw.Write(r.CenterX + "," + r.CenterY + "\n" );
            }

            sw.Write("\n\n\n");

        }
        sw.Close();

        File.WriteAllBytes(filename + ".png", dungeonMap.EncodeToPNG());
        #endif
         
    }

    /// <summary>
    /// Generates a texture containing the rooms based on a number of rooms.
    /// Rooms do not overlap and are written directly into the texture
    /// </summary>
    /// <param name="noRooms">number of rooms to be generated</param>
    /// <returns></returns>
    private Texture2D GenerateDungeonMap(int noRooms)
    {
        dungeonMap = Generator.clearTexture(dungeonMap, Color.white);

        // parameters for room. 
        int posX = 0; // starting bottom left of texture
        int posY = 0;
        int width = 0;
        int height = 0;
        int i = 0;
        int roomBorder = 5;
        bool overlay = false;

        do
        {
            overlay = false;
            width = UnityEngine.Random.Range(30, 50);
            height = UnityEngine.Random.Range(30, 50);

            posX = UnityEngine.Random.Range(roomBorder, dungeonMap.width - width - roomBorder);
            posY = UnityEngine.Random.Range(roomBorder, dungeonMap.height - height - roomBorder);

            for (int x = 0; x < width+(roomBorder*2); x++)
            {
                for (int y = 0; y < height+(roomBorder*2); y++)
                {
                    Color c = dungeonMap.GetPixel(posX + x, posY + y);
                    if ( c == Color.black || c == Color.red)
                    {
                        overlay = true;
                        break;
                    }
                }
                if (overlay) break;
            }

            if (overlay) continue;

            for (int x = 0; x < width+(roomBorder*2); x++)
            {
                for (int y = 0; y < height+(roomBorder*2); y++)
                {
                    if (x < roomBorder || x > width + roomBorder || y < roomBorder || y > width + roomBorder)
                    {
                        dungeonMap.SetPixel(posX + x, posY + y, Color.red);
                    }
                    else
                    {
                        dungeonMap.SetPixel(posX + x, posY + y, Color.black); 
                    }   
                }
            }
            i++;

        } while (i < noRooms);

#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("dungeonmap_wborders.png", dungeonMap.EncodeToPNG());
#endif

        // delete borders
        for (int x = 0; x < dungeonMap.width; x++)
        {
            for (int y = 0; y < dungeonMap.height; y++)
            {
                if (dungeonMap.GetPixel(x, y) == Color.red)
                {
                    dungeonMap.SetPixel(x, y, Color.white);
                }
            }
        }


#if UNITY_EDITOR && !UNITY_WEBPLAYER
            File.WriteAllBytes("dungeonmap.png", dungeonMap.EncodeToPNG());
#endif

        return dungeonMap;
    }

    /// <summary>
    /// Generates a texture containing the rooms based on a list of previously generated room
    /// </summary>
    /// <param name="roomList">List of rooms as list of integer arrays</param>
    /// <returns></returns>
    private bool GenerateDungeonMap(List<Room> roomList, Texture2D dungeonMap)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            Color[] clist = new Color[(int) (  (roomList[i].LowerLeftX - roomList[i].UpperRightX) 
                                             * (roomList[i].LowerLeftY - roomList[i].UpperRightY))];

            try
            {
                dungeonMap.SetPixels((int)roomList[i].LowerLeftX,
                                          (int)roomList[i].LowerLeftY,
                                          (int)(roomList[i].UpperRightX - roomList[i].LowerLeftX),
                                          (int)(roomList[i].UpperRightY - roomList[i].LowerLeftY),
                                          clist);
            }
            //catch (IndexOutOfRangeException)
            catch(Exception e)
            {
                Debug.Log(e);
                Debug.Log("GenerateDungeonMap: Found a Room out of bounds");
#if UNITY_EDITOR && !UNITY_WEBPLAYER
                DebugSaveDungeon(roomList, dungeonMap, true);
#endif
                return false;
                //throw;
            }
        }

        return true;
    }

    /// <summary>
    /// Generates a number of rectangles defined by lower right and upper left point to be used to generate a dungeon
    /// Known Error: sometimes a room is still out of bounds, currently caught by Exception when drawing them on the dungeonMap 
    /// </summary>
    /// <param name="dungeonWidth">Width of texture used for mesh generation</param>
    /// <param name="dungeonHeight">Height of texture used for mesh generation</param>
    /// <param name="noRooms">number of rooms to be generated</param>
    /// <returns>List of rooms as list of integer arrays, containing coordinates in this order: lower left corner (x and y), upper right corder (x and y), center (x and y)</returns>
    private List<Room> GenerateDungeonRooms(int dungeonWidth, int dungeonHeight, int noRooms, int roomBorder = 5, int roomSideMin = 10, int roomSideMax = 30)
    {
        List<Room> roomList = new List<Room>();

        int i = 0;
        int posX = 0;
        int posY = 0;
        int width = 0;
        int height = 0;

        bool found = false; // flag that shows if a suitable place for a room is found
        bool complete = false; // flag that shows if all other rooms are checked for overlaps
        bool outofbounds = false; // flag that show if room is about to go beyond the size of the texture

        do
        {
            found = false;
            complete = false;
            width = UnityEngine.Random.Range(roomSideMin, roomSideMax);
            height = UnityEngine.Random.Range(roomSideMin, roomSideMax);

            int x = roomBorder, y = roomBorder;

            do
            {
                /* Beide Fälle treffen nie zu, auch wenn ein raum zu groß wird
                 * 
                 */
                if ((x + width + (roomBorder * 2)) > dungeonWidth)
                {
                    x -= roomSideMin;
                    Debug.Log("Room out of Bounds X: " + (x + width + (roomBorder * 2)));
                    outofbounds = true;
                    break;
                }

                if ((y + height + (roomBorder * 2)) > dungeonHeight)
                {
                    y -= roomSideMin;
                    Debug.Log("Room out of Bounds Y: " + (y + width + (roomBorder * 2)));
                    outofbounds = true;
                    break;
                }

                do
                {
                    if (roomList.Count == 0)
                    {
                        posX = x;
                        posY = y;
                        found = true;
                        complete = true;
                        break;
                    }

                    for (int r = 0; r < roomList.Count; r++ )
                    {
                        if (overlapsWithExistingRoom(roomList[r], roomBorder, x, y, width, height))
                        {
                            // x/y overlap with current room
                            if(UnityEngine.Random.Range(0.0f, 1.0f) > 0.5f) // overlaps
                            {
                                 x += roomSideMin;
                                    
                            }
                            else
                            {
                                 y += roomSideMin;
                            }
                            
                            found = false;
                            break;
                        }
                        else
                        {
                            // found a suitable spot
                            posX = x;
                            posY = y;
                            found = true;
                        }

                        // only done when every room is checked
                        if (r == (roomList.Count - 1) && found)
                        {
                            complete = true;   
                        }
                    }

                    if (found && complete) break;
                } while (y < dungeonHeight - roomBorder);
                if (found && complete) break;
            } while (x < dungeonWidth - roomBorder);

            
            if (!outofbounds)
            {
                roomList.Add(new Room(posX, posY, posX + width, posY + height));
                i++; 
            }
        } while (i < noRooms);

        return roomList;
    }

    /// <summary>
    /// Checks if a new room overlaps with an existing one
    /// </summary>
    /// <param name="r">existing room</param>
    /// <param name="roomBorder">padding around rooms</param>
    /// <param name="x">lower left x of new room</param>
    /// <param name="y">lower left y of new room</param>
    /// <param name="w">width of new room</param>
    /// <param name="h">height of new room</param>
    /// <returns>true if new room overlaps with existing room; false if not</returns>
    private static bool overlapsWithExistingRoom(Room r, int roomBorder, int x, int y, int w, int h)
    {
        if ((x     > r.LowerLeftX - roomBorder && x     < r.UpperRightX + roomBorder) && (y     > r.LowerLeftY - roomBorder && y     < r.UpperRightY + roomBorder)) return true;
        if ((x + w > r.LowerLeftX - roomBorder && x + w < r.UpperRightX + roomBorder) && (y     > r.LowerLeftY - roomBorder && y     < r.UpperRightY + roomBorder)) return true;
        if ((x     > r.LowerLeftX - roomBorder && x     < r.UpperRightX + roomBorder) && (y + h > r.LowerLeftY - roomBorder && y + h < r.UpperRightY + roomBorder)) return true;
        if ((x + w > r.LowerLeftX - roomBorder && x + w < r.UpperRightX + roomBorder) && (y + h > r.LowerLeftY - roomBorder && y + h < r.UpperRightY + roomBorder)) return true;

        return false;
    }

    /// <summary>
    /// Generate corridors between rooms based on neighbours
    /// </summary>
    /// <param name="roomList">List of rooms</param>
    /// <param name="dungeonMap">Texture representing dungeon</param>
    private void GenerateCorridor(List<Room> roomList, Texture2D dungeonMap, int CorridorWidth = 3)
    {
        foreach (Room r in roomList)
        {
            foreach (Room n in r.Neighbours)
            {
                if (r.CenterX < n.CenterX)
                {
                    if (r.CenterY < n.CenterY)
                    {
                        //REFACTOR setpixel() -> setpixels() -> go fast!

                        // horizontal line
                       
                        for (int x = r.CenterX; x <= n.CenterX; x++)
                        {
                            for (int y = n.CenterY; y <= n.CenterY + CorridorWidth; y++)
                            {
                                dungeonMap.SetPixel(x, y, Color.black);
                            }
                        }
                        
                        //dungeonMap.SetPixels(r.CenterX, r.CenterY, n.CenterX - r.CenterX, CorridorWidth, 
                        //                     Generator.GenerateSingleColorArray(r.CenterX * (n.CenterX - r.CenterX) + r.CenterY * CorridorWidth, Color.black));

                        // vertical line
                        
                        for (int x = r.CenterX; x <= r.CenterX + CorridorWidth; x++)
                        {
                            for(int y = r.CenterY; y <= n.CenterY; y++)
                            {
                                dungeonMap.SetPixel(x,y,Color.black);
                            }
                        }
                        
                        //dungeonMap.SetPixels(r.CenterX, r.CenterY, CorridorWidth, n.CenterY-r.CenterY, 
                        //                     Generator.GenerateSingleColorArray(r.CenterX * CorridorWidth + r.CenterY * (n.CenterY - r.CenterY), Color.black));

                    }
                    else
                    {
                        // horizontal line
                        
                        for (int x = r.CenterX; x <= n.CenterX; x++)
                        {
                            for (int y = n.CenterY; y <= n.CenterY + CorridorWidth; y++)
                            {
                                dungeonMap.SetPixel(x, y, Color.black);
                            }
                        } 
                        
                        //dungeonMap.SetPixels(r.CenterX, n.CenterY, n.CenterX - r.CenterX, CorridorWidth, 
                        //                     Generator.GenerateSingleColorArray(r.CenterX * (n.CenterX - r.CenterX) + n.CenterY * CorridorWidth, Color.black));

                        // vertical line
                        
                        for (int x = r.CenterX; x <= r.CenterX + CorridorWidth; x++)
                        {
                            for (int y = r.CenterY; y >= n.CenterY; y--)
                            {
                                dungeonMap.SetPixel(x, y, Color.black); 
                            }
                        }
                        
                        //dungeonMap.SetPixels(r.CenterX, r.CenterY, CorridorWidth, r.CenterY - n.CenterY,
                        //                     Generator.GenerateSingleColorArray(r.CenterX * CorridorWidth + r.CenterY * (r.CenterY - n.CenterY), Color.black));
                        
                    }
                }
                else
                {
                    if (r.CenterY < n.CenterY)
                    {                          
                        // horizontal line
                        
                        for (int x = n.CenterX; x <= r.CenterX; x++)
                        {
                            for (int y = r.CenterY; y <= r.CenterY + CorridorWidth; y++)
                            {
                                dungeonMap.SetPixel(x, y, Color.black);
                            }
                        }
                        
                        //dungeonMap.SetPixels(n.CenterX, r.CenterY, r.CenterX - n.CenterX, CorridorWidth,
                         //                    Generator.GenerateSingleColorArray(n.CenterX * (r.CenterX - n.CenterX) + r.CenterY * CorridorWidth, Color.black));

                        // vertical line
                        
                        for (int x = n.CenterX; x <= n.CenterX + CorridorWidth; x++)
                        {
                            for (int y = n.CenterY; y <= r.CenterY; y++)
                            {
                                dungeonMap.SetPixel(x, y, Color.black);
                            }
                        }
                        
                        //dungeonMap.SetPixels(n.CenterX, n.CenterY, CorridorWidth, n.CenterY - r.CenterY,
                         //                    Generator.GenerateSingleColorArray(n.CenterX * CorridorWidth + n.CenterY * (n.CenterY - r.CenterY), Color.black)); 

                    }
                    else
                    {
                        // horizontal line
                        
                        for (int x = n.CenterX; x <= r.CenterX; x++)
                        {
                            for (int y = r.CenterY; y <= r.CenterY + CorridorWidth; y++)
                            {
                                dungeonMap.SetPixel(x, y, Color.black);
                            }
                        }
                        
                        //dungeonMap.SetPixels(n.CenterX, r.CenterY, (r.CenterX - n.CenterX), CorridorWidth,
                        //                     Generator.GenerateSingleColorArray(n.CenterX * (r.CenterX - n.CenterX) + r.CenterY * CorridorWidth, Color.black));

                        // vertical line
                        
                        for (int x = n.CenterX; x <= n.CenterX + CorridorWidth; x++)
                        {
                            for (int y = n.CenterY; y >= r.CenterY; y--)
                            {
                                dungeonMap.SetPixel(x, y, Color.black);
                            }
                        }
                        
                        //dungeonMap.SetPixels(n.CenterX, n.CenterY, CorridorWidth, r.CenterY - n.CenterY,
                        //                     Generator.GenerateSingleColorArray(n.CenterX * CorridorWidth + n.CenterY * (r.CenterY - n.CenterY), Color.black));
                        
                    }

                }


            }
        }
        #if UNITY_EDITOR && !UNITY_WEBPLAYER
            File.WriteAllBytes("dungeon_withcorridors.png", dungeonMap.EncodeToPNG());
        #endif
    }    

    /// <summary>
    /// DEBUG: generates fixed roomes
    /// </summary>
    /// <param name="noRooms">Number of rooms to be generated</param>
    /// <returns>List of rooms</returns>
    private List<Room> debugRoomList(int noRooms = 5)
    {
        List<Room> roomlist = new List<Room>();

        Room r0 = new Room(5, 5, 27, 34);
        Room r1 = new Room(35, 5, 52, 29);
        Room r2 = new Room(35, 35, 45, 50);
        Room r3 = new Room(45, 55, 67, 65);

        r0.Neighbours.Add(r1);

        r1.Neighbours.Add(r0);
        r1.Neighbours.Add(r2);

        r2.Neighbours.Add(r1);
        r2.Neighbours.Add(r3);

        r3.Neighbours.Add(r2);

        roomlist.Add(r0);
        roomlist.Add(r1);
        roomlist.Add(r2);
        roomlist.Add(r3);

        return roomlist;
    }

    private void debugDrawRoomsAndNodes(List<Room> roomlist)
    {
        Texture2D tex = new Texture2D(dungeonMap.width, dungeonMap.height, TextureFormat.RGB24, false);
        for (int i = 0; i < roomlist.Count; i++)
        {
            tex.SetPixel(roomlist[i].CenterX, roomlist[i].CenterY, Color.blue);
        }
#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("dungeon_rooms.png", dungeonMap.EncodeToPNG());
#endif
#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("dungeon_nodes.png", tex.EncodeToPNG());
#endif

        Texture2D tex2 = new Texture2D(dungeonMap.width, dungeonMap.height, TextureFormat.RGB24, false);
        for(int x=0;x<dungeonMap.width;x++){
            for(int y=0;y<dungeonMap.height;y++){
                tex2.SetPixel(x, y, dungeonMap.GetPixel(x, y));
            }
        }

        for (int i = 0; i < roomlist.Count; i++)
        {
            foreach (Room r in roomlist[i].Neighbours)
            {
                Generator.DrawLine(tex2, roomlist[i].CenterX, roomlist[i].CenterY, r.CenterX, r.CenterY, Color.cyan);
            }
        }
#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("dungeon_nodes-edges.png", tex2.EncodeToPNG());
#endif
#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("dungeon_withcorridors.png", dungeonMap.EncodeToPNG());
#endif
    }
}
