﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Class defining a node in the mission graph
/// </summary>
public class MissionNode {
    private string m_task;
    private int [] m_point;
    private List<MissionNode> m_next;
    private bool m_completed;

    public MissionNode(string t)
    {
        m_task = t;
        m_next = new List<MissionNode>();
        m_completed = false;
        m_point = new int[2];
    }
    public bool Completed
    {
        get { return m_completed; }
        set { m_completed = value; }
    }

    public string Task
    {
        get { return m_task;  }
        set { m_task = value; }
    }

    
    public int[] Point
    {
        get { return m_point;  }
        set { m_point = value; }
    }

    public int PointX
    {
        get { return m_point[0]; }
        set {m_point[0] = value;}
    }

    public int PointY
    {
        get { return m_point[1]; }
        set { m_point[1] = value; }
    }

    public List<MissionNode> Next
    {
        get { return m_next;  }
        set { m_next = value; }
    }

    public void AddNext(MissionNode node)
    {
        this.m_next.Add(node);
    }

    public bool IsStartNode(MissionNode n)
    {
        return n.IsStartNode();
    }

    public bool IsStartNode()
    {
        if (this.Task == "S") return true;

        return false;
    }

    public bool IsEndNode(MissionNode n)
    {
        return n.IsEndNode();
    }

    public bool IsEndNode()
    {
        if (this.Task == "E") return true;

        return false;
    }

    public bool IsTaskNode(MissionNode n)
    {
        return n.IsTaskNode();
    }

    public bool IsTaskNode()
    {
        if (this.Task == "K") return true;
        if (this.Task == "F") return true;
        if (this.Task == "Ke") return true;
        if (this.Task == "Od") return true;
        if (this.Task == "Oc") return true;

        return false;
    }

    public bool IsTaskEndNode(MissionNode n)
    {
        return n.IsTaskEndNode();
    }

    public bool IsTaskEndNode()
    {
        if (this.Task == "C") return true;
        if (this.Task == "Kb") return true;

        return false;
    }
    public void SetPoint(int x, int y)
    {
        this.m_point[0] = x;
        this.m_point[1] = y;
    }

    public int GetPointX()
    {
        return this.m_point[0];
    }

    public int GetPointY()
    {
        return this.m_point[1];
    }


}
