﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class MissionGraph{
    private List<MissionNode> m_nodes;
    private List<string> m_constantTypes;
    //private bool m_allObjectivesFinished;

    /// <summary>
    /// Constructor
    /// </summary>
    /// <param name="mission">Generated mission as string with spaces as delimiter</param>
    public MissionGraph(string mission)
    {
        //m_allObjectivesFinished = false;
        m_nodes = new List<MissionNode>();
        m_constantTypes = new List<string>();

        string[] splitMissions = mission.Split(new char[] { ' ' });
        for (int i = 0; i < splitMissions.Length; i++)
        {
            if (splitMissions[i].Trim() != "")
            {
                MissionNode node = new MissionNode(splitMissions[i]);
                m_nodes.Add(node);
            }
        }

        for (int i = 0; i < m_nodes.Count; i++)
        {
            if (!m_constantTypes.Contains(m_nodes[i].Task)) m_constantTypes.Add(m_nodes[i].Task);

            switch (m_nodes[i].Task)
            {
                // start node
                case "S":
                    {
                        for (int j = 0; j < m_nodes.Count; j++)
                        {
                            if (m_nodes[j].IsTaskNode()) m_nodes[i].AddNext(m_nodes[j]);
                        }
                        m_nodes[i].Completed = true;
                        break;
                    }

                // nodes connected to each other without other node ending the task
                case "F":
                case "Od":
                case "Oc":
                case "Ke":
                    {
                        for (int j = 0; j < m_nodes.Count; j++)
                        {
                            if (m_nodes[i].Task == m_nodes[j].Task) m_nodes[i].AddNext(m_nodes[j]);

                            if (m_nodes[j].IsEndNode()) m_nodes[i].AddNext(m_nodes[j]);
                        }
                        break;
                    }

                // nodes connected to each other with other node ending the task
                case "K":
                    {
                        for (int j = 0; j < m_nodes.Count; j++)
                        {
                            if (m_nodes[i].Task == m_nodes[j].Task) m_nodes[i].AddNext(m_nodes[j]);

                            if (m_nodes[j].IsTaskEndNode()) m_nodes[i].AddNext(m_nodes[j]);
                        } 
                        break;
                    }

                // nodes ending a task
                case "C":
                case "Kb":
                    {
                        for (int j = 0; j < m_nodes.Count; j++)
                        {
                            if (m_nodes[j].IsEndNode()) m_nodes[i].AddNext(m_nodes[j]);
                        }

                        break;
                    }

                // End node
                case "E":
                    {
                        m_nodes[i].Completed = true; 
                        break;
                    }
            }
        }
        //Debug.Log("End of Mission Graph generation");
    }

    /// <summary>
    /// calculates points of missions for sketch
    /// </summary>
    /// <param name="width">Width of noise texture</param>
    /// <param name="heigt">Height of noise texture</param>
    public void calculateSketchPoints(int width, int heigt)
    {
        //int noConstantTypes = m_constantTypes.Count;
        int noNodes = m_nodes.Count;

        string prevType = "";
        int segment = -1;
        int segmentWidth = Mathf.FloorToInt(width / m_constantTypes.Count);
        for (int i = 0; i < noNodes; i++)
        {
            if (prevType != m_nodes[i].Task /*&& prevType != ""*/) segment++;

            int x = UnityEngine.Random.Range(segment * segmentWidth, (segment + 1) * segmentWidth);
            int y = UnityEngine.Random.Range(10, heigt - 10);

            this.m_nodes[i].Point = new int[2]{x, y};
            prevType = m_nodes[i].Task;
        }
    }

    /// <summary>
    /// Checks if all current objectives are finished
    /// </summary>
    /// <returns>false if an uncompleted objective is found; otherwise true</returns>
    public bool CheckObjectivesFinished()
    {
        for (int i = 0; i < this.m_nodes.Count; i++)
        {
            if (!this.m_nodes[i].Completed) return false;
        }

        return true;
    }

    public List<MissionNode> GetNodes()
    {
        return this.m_nodes;
    }

    public List<string> GetConstantTypes()
    {
        return this.m_constantTypes;
    }

    public int GetNumberOfConstantTypes()
    {
        return this.m_constantTypes.Count;
    }

    internal Texture2D DrawMissionSketch(int width, int height, Color c)
    {
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);

        for (int i = 0; i < this.m_nodes.Count; i++)
        {
            for (int j = 0; i < this.m_nodes[i].Next.Count; i++)
            {
                Generator.DrawLine(tex, (int)m_nodes[i].PointX,         (int)m_nodes[i].PointY, 
                                        (int)m_nodes[i].Next[j].PointX, (int)m_nodes[i].Next[j].PointY, 
                                        c);
            }
        }

        return tex;
    }

    /// <summary>
    /// Generates line segments for sketch based on mission
    /// </summary>
    /// <returns>List of points defining the segments</returns>
    internal List<Vector2> GenerateLineSegments()
    {
        List<Vector2> lineSegments = new List<Vector2>();

        for (int i = 0; i < this.m_nodes.Count; i++)
        {
            for (int j = 0; j < this.m_nodes[i].Next.Count; j++)
            {
                lineSegments.Add(new Vector2(this.m_nodes[i].PointX,         this.m_nodes[i].PointY));
                lineSegments.Add(new Vector2(this.m_nodes[i].Next[j].PointX, this.m_nodes[i].Next[j].PointY));
            }
        }

        return lineSegments;
    }
}
