﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;

/// <summary>
/// General generator class. Provides functions usefull to all other, more specific generator classes
/// </summary>
public class Generator : MonoBehaviour {


    //GameObject m_gameobject;

    //private Texture2D noiseTexture;
    public Material material; 
    public Color color;
    public Vector3 meshSize = new Vector3(200, 30, 200);
    public bool continuousUpdate = false;

    /// <summary>
    /// Perlin noise generator provied by Unity Procedural Example
    /// </summary>
    private PerlinBasic perlin;

    /// <summary>
    /// Fractal noise generator provided by Unity Procedural Example
    /// </summary>
    private FractalNoise fractal;

    // general generation parameters
    public bool gray = true;
    public int noiseWidth = 128;
    public int noiseHeight = 128;
    
    // parameters for perlin noise
    public float octaves = 8.379f;
    public float lacunarity = 6.18f;
    public float h = 0.69f;
    public float offset = 0.75f;
    public float scale = 0.09f;
    public float offsetPos = 0.0f;

    /// <summary>
    /// Generates a noise texture using perlin noise and hybrid multi fractal
    /// </summary>
    /// <returns>2D grayscale texture with noise</returns>
    public Texture2D GenerateNoiseTexture()
    {
        Texture2D noiseTexture = new Texture2D(noiseWidth, noiseHeight, TextureFormat.RGB24, false);

        if (perlin == null)
        {
            perlin = new PerlinBasic();
        }

        fractal = new FractalNoise(h, lacunarity, octaves, perlin);

        for (int x = 0; x < noiseWidth; x++)
        {
            for (int y = 0; y < noiseHeight; y++)
            {
                if (gray)
                {
                    float value = fractal.HybridMultifractal(x * scale + Time.time, y * scale + Time.time, offset);
                    noiseTexture.SetPixel(x, y, new Color(value, value, value, value));
                }
                else
                {
                    offsetPos = Time.time;
                    float valueX = fractal.HybridMultifractal(x * scale + offsetPos * 0.6f, y * scale + offsetPos * 0.6f, offset);
                    float valueY = fractal.HybridMultifractal(x * scale + 161.7f + offsetPos, y * scale + 161.7f + offsetPos * 0.3f, offset);
                    float valueZ = fractal.HybridMultifractal(x * scale + 591.1f + offsetPos, y * scale + 591.1f + offsetPos * 0.1f, offset);
                    noiseTexture.SetPixel(x, y, new Color(valueX, valueY, valueZ, 1));
                }
            }
        }
        #if UNITY_EDITOR && !UNITY_WEBPLAYER
            File.WriteAllBytes("noisetexture_aftergeneration_perlinbasic.png", noiseTexture.EncodeToPNG());
        #endif

        return noiseTexture;
    }

    /// <summary>
    /// Generates heightmap to use with Unity terrain tool using perlin noise and hybrid multi fractal
    /// </summary>
    /// <param name="width">width of terrain component</param>
    /// <param name="height">height of terrain component</param>
    /// <returns>2D array of floats representing the height values</returns>
    public float[,] GenerateNoiseHeightmap(int width, int height)
    {
        float[,] hm = new float[width, height];

        if (perlin == null)
        {
            perlin = new PerlinBasic();
        }

        fractal = new FractalNoise(h, lacunarity, octaves, perlin);
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                hm[x, y] = fractal.HybridMultifractal(x * scale + Time.time, y * scale + Time.time, offset);
            }
        }

        return hm;
    }

    /// <summary>
    /// Genereates a mesh based on a noise texture.
    /// The gray value indicates the height of a vertice in z-axis.
    /// Texture is scaled to size of the mesh.
    /// </summary>
    /// <param name="noiseTexture">Noise texture used for generation</param>
    public void GenerateGeometryFromHeightmap(Texture2D noiseTexture)
    {
        renderer.material.mainTexture = null;
        if (material != null)
        {
            renderer.material = material;
        }
        else
        {
            renderer.material.color = color;
        }


        Mesh mesh = GetComponent<MeshFilter>().mesh;
        int width = Mathf.Min(noiseTexture.width, 255);
        int height = Mathf.Min(noiseTexture.height, 255);
        int x = 0, y = 0;

        Vector3[] vertices = new Vector3[width * height];
        Vector2[] uv = new Vector2[height * width];
        Vector4[] tangents = new Vector4[height * width];

        Vector2 uvScale = new Vector2(1.0f / (width - 1), 1.0f / (height - 1));
        Vector3 sizeScale = new Vector3(meshSize.x / (width - 1), meshSize.y, meshSize.z / (height - 1));

        for (x = 0; x < width; x++)
        {
            for (y = 0; y < height; y++)
            {
                float pixelHeight = noiseTexture.GetPixel(x, y).grayscale;
                Vector3 vertex = new Vector3(x, pixelHeight, y);
                vertices[y * width + x] = Vector3.Scale(sizeScale, vertex);
                uv[y * width + x] = Vector2.Scale(new Vector2(x, y), uvScale);

                // Calculate tangent vector, used with bumpmap shaders on mesh
                Vector3 vertexL = new Vector3(x - 1, noiseTexture.GetPixel(x - 1, y).grayscale);
                Vector3 vertexR = new Vector3(x + 1, noiseTexture.GetPixel(x + 1, y).grayscale);
                Vector3 tan = Vector3.Scale(sizeScale, vertexR - vertexL).normalized;
                tangents[y * width + x] = new Vector4(tan.x, tan.y, tan.z, -1.0f);
            }
        }

        mesh.vertices = vertices;
        mesh.uv = uv;

        // build triangle indices
        int[] triangles = new int[(height - 1) * (width - 1) * 6];
        int index = 0;

        for (x = 0; x < width - 1; x++)
        {
            for (y = 0; y < height - 1; y++)
            {
                triangles[index++] = (y * width) + x;
                triangles[index++] = ((y + 1) * width) + x;
                triangles[index++] = (y * width) + x + 1;

                triangles[index++] = ((y + 1) * width) + x;
                triangles[index++] = ((y + 1) * width) + x + 1;
                triangles[index++] = (y * width) + x + 1;
            }
        }

        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        mesh.tangents = tangents;
    }

    /// <summary>
    /// Constructor.
    /// </summary>
    /// <param name="gameobject">Current game object</param>
    public Generator(GameObject gameobject)
    {

    }

    /// <summary>
    /// Fills texture with default color gray (0.5f,0.5f,0.5f)
    /// </summary>
    /// <param name="tex">Texture to be filled</param>
    /// <returns>Filled texture</returns>
    public static Texture2D clearTexture(Texture2D tex)
    {
        return clearTexture(tex, new Color(0.5f, 0.5f, 0.5f));
    }

    /// <summary>
    /// Fills texture with specified color
    /// </summary>
    /// <param name="tex">Texture to be filled</param>
    /// <param name="color">color</param>
    /// <returns>Filled texture</returns>
    public static Texture2D clearTexture(Texture2D tex, Color color)
    {
        if (tex == null) throw new ArgumentException("clearTexutre: Texture not initilized");

        for (int x = 0; x < tex.width; x++)
        {
            for (int y = 0; y < tex.height; y++)
            {
                tex.SetPixel(x, y, color); // slow for many operations
            }
        }

        return tex;
    }

    public static Texture2D clearTextureFast(Texture2D tex)
    {
        return clearTextureFast(tex, new Color(0.5f, 0.5f, 0.5f));
    }

    /// <summary>
    /// Fills texture with specified color
    /// Uses Texture2D.SetPixels to improve performance
    /// </summary>
    /// <param name="tex">texture to be filled</param>
    /// <param name="color">color</param>
    /// <returns>filled texture</returns>
    public static Texture2D clearTextureFast(Texture2D tex, Color color)
    {
        if (tex == null) throw new ArgumentException("clearTexutreFast: Texture not initilized");
        tex.SetPixels(0, 0, tex.width, tex.height, GenerateSingleColorArray(tex.width * tex.height, color));
        return tex;
    }

    /// <summary>
    /// Converts a grayscale 2d texture to a 2d array of floats
    /// </summary>
    /// <param name="texture">grayscale texture (if not already values will be converted)</param>
    /// <returns>2d float array</returns>
    public static float[,] convertHeightmap(Texture2D texture)
    {
        int width = texture.width;
        int height = texture.height;
        float[,] array = new float[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                array[x,y] = texture.GetPixel(x, y).grayscale;
            }
        }

        return array;
    }

    /// <summary>
    /// Converts a 2d array of floats to a grayscale texture
    /// </summary>
    /// <param name="array">2d array of floats</param>
    /// <returns>grayscale texture 2d</returns>
    public static Texture2D convertHeightmap(float[,] array)
    {
        int width = array.GetLength(0);
        int height = array.GetLength(1);
        Texture2D texture = new Texture2D(width, height, TextureFormat.RGB24, false);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                //Color c = new Color(array[x,y],array[x,y],array[x,y]);
                texture.SetPixel(x, y, new Color(array[x, y], array[x, y], array[x, y]));
            }
        }

        return texture;
    }

    /// <summary>
    /// Generates a array of colors with only one color, as requiered for SetPixels()-function
    /// </summary>
    /// <param name="length">size of array</param>
    /// <param name="color">color with which to fill array</param>
    /// <returns>array of colors</returns>
    public static Color[] GenerateSingleColorArray(int length, Color color){
        Color [] c = new Color[length];
        for(int i = 0; i < length; i++){
            c[i] = color;
        }

        return c;
    }

    /// <summary>
    /// Add color values of two textures
    /// </summary>
    /// <param name="t1">first texture</param>
    /// <param name="t2">second texture</param>
    /// <returns>new texture</returns>
    public static Texture2D AddTextures(Texture2D t1, Texture2D t2)
    {
        if(t1.width != t2.width || t1.height != t2.height){
            throw new ArgumentException("AddTextures: Sizes of texture do not match.");
        }

        Texture2D addtex = new Texture2D(t1.width, t1.height, TextureFormat.RGB24, false);
        for (int x = 0; x < t1.width; x++)
        {
            for (int y = 0; y < t1.height; y++)
            {
                //Color c = t1.GetPixel(x,y) + t2.GetPixel(x,y);
                addtex.SetPixel(x, y, (t1.GetPixel(x,y) + t2.GetPixel(x,y)));
            }
        }
        return addtex;
    }

    /// <summary>
    /// Add color values of two textures only at coordinates where the second texture is filled with a key color.
    /// </summary>
    /// <param name="t1">first texture</param>
    /// <param name="t2">second texture</param>
    /// <param name="key">key color</param>
    /// <returns>new texture</returns>
    public static Texture2D AddTexturesKey(Texture2D t1, Texture2D t2, Color key)
    {
        if (t1.width != t2.width || t1.height != t2.height)
        {
            throw new ArgumentException("AddTextures: Sizes of texture do not match.");
        }

        Texture2D addtex = new Texture2D(t1.width, t1.height, TextureFormat.RGB24, false);
        for (int x = 0; x < t1.width; x++)
        {
            for (int y = 0; y < t1.height; y++)
            {
                if (t2.GetPixel(x, y) == key)
                {
                    addtex.SetPixel(x, y, (t1.GetPixel(x, y) + t2.GetPixel(x, y)));
                }
                else
                {
                    addtex.SetPixel(x, y, t1.GetPixel(x, y));
                }
                
            }
        }
        return addtex;
    }

    /// <summary>
    /// Subtract color values of second texture from first texture
    /// </summary>
    /// <param name="t1">first texture</param>
    /// <param name="t2">second texture</param>
    /// <returns>new texture</returns>
    public static Texture2D SubTextures(Texture2D t1, Texture2D t2)
    {
        if (t1.width != t2.width || t1.height != t2.height)
        {
            throw new ArgumentException("AddTextures: Sizes of texture do not match.");
        }

        Texture2D addtex = new Texture2D(t1.width, t1.height, TextureFormat.RGB24, false);
        for (int x = 0; x < t1.width; x++)
        {
            for (int y = 0; y < t1.height; y++)
            {
                //Color c = t1.GetPixel(x, y) - t2.GetPixel(x, y);
                addtex.SetPixel(x, y, (t1.GetPixel(x, y) - t2.GetPixel(x, y)));
            }
        }
        return addtex;
    }

    /// <summary>
    /// Subtract color values of second texture from first texture only at coordinates where the second texture has a key color
    /// </summary>
    /// <param name="t1">first texture</param>
    /// <param name="t2">second texture</param>
    /// <param name="key">key color</param>
    /// <returns>new texture</returns>
    public static Texture2D SubTexturesKey(Texture2D t1, Texture2D t2, Color key)
    {
        if (t1.width != t2.width || t1.height != t2.height)
        {
            throw new ArgumentException("AddTextures: Sizes of textures do not match.");
        }

        Texture2D addtex = new Texture2D(t1.width, t1.height, TextureFormat.RGB24, false);
        for (int x = 0; x < t1.width; x++)
        {
            for (int y = 0; y < t1.height; y++)
            {
                if (t2.GetPixel(x, y) == key)
                {
                    addtex.SetPixel(x, y, (t1.GetPixel(x, y) - t2.GetPixel(x, y)));
                }
                else
                {
                    addtex.SetPixel(x, y, t1.GetPixel(x, y));
                }
                    
            }
        }
        return addtex;
    }

    // usefull for Mission to Shape Grammer conversion?
    /// <summary>
    /// Draws a 1pixel wide line between two points.
    /// Source: http://wiki.unity3d.com/index.php?title=TextureDrawLine
    /// </summary>
    /// <param name="tex">Texturen</param>
    /// <param name="x0">x-coordinate of starting point</param>
    /// <param name="y0">y-coordinate of starting point</param>
    /// <param name="x1">x-coordinate of ending point</param>
    /// <param name="y1">y-coordinate of ending point</param>
    /// <param name="col">Color of the line</param>
    public static void DrawLine(Texture2D tex, int x0, int y0, int x1, int y1, Color col)
    {
        int dy = (int)(y1 - y0);
        int dx = (int)(x1 - x0);
        int stepx, stepy;

        if (dy < 0) { dy = -dy; stepy = -1; }
        else { stepy = 1; }
        if (dx < 0) { dx = -dx; stepx = -1; }
        else { stepx = 1; }
        dy <<= 1;
        dx <<= 1;

        float fraction = 0;

        tex.SetPixel(x0, y0, col);
        if (dx > dy)
        {
            fraction = dy - (dx >> 1);
            while (Mathf.Abs(x0 - x1) > 1)
            {
                if (fraction >= 0)
                {
                    y0 += stepy;
                    fraction -= dx;
                }
                x0 += stepx;
                fraction += dy;
                tex.SetPixel(x0, y0, col);
            }
        }
        else
        {
            fraction = dx - (dy >> 1);
            while (Mathf.Abs(y0 - y1) > 1)
            {
                if (fraction >= 0)
                {
                    x0 += stepx;
                    fraction -= dy;
                }
                y0 += stepy;
                fraction += dx;
                tex.SetPixel(x0, y0, col);
            }
        }
    }

    /// <summary>
    /// Set for every pixel with keycolor in t2 pixel in t1 to value of t2
    /// </summary>
    /// <param name="t1">texture to be drawn on</param>
    /// <param name="t2">texture including keyed area</param>
    /// <param name="key">key color</param>
    /// <returns>t1 including values from t2 at keyed area</returns>
    public static Texture2D SetTexturesKey(Texture2D t1, Texture2D t2, Color key)
    {
        if (t1.width != t2.width || t1.height != t2.height)
        {
            throw new ArgumentException("AddTextures: Sizes of texture do not match.");
        }

        Texture2D addtex = new Texture2D(t1.width, t1.height, TextureFormat.RGB24, false);
        for (int x = 0; x < t1.width; x++)
        {
            for (int y = 0; y < t1.height; y++)
            {
                if (t2.GetPixel(x, y) == key)
                {
                    addtex.SetPixel(x, y, t2.GetPixel(x, y));
                }
                else
                {
                    addtex.SetPixel(x, y, t1.GetPixel(x, y));
                }

            }
        }
        return addtex;
    }

    /// <summary>
    /// Set every pixel in t1 to specified color at position specified in t2 through key color
    /// </summary>
    /// <param name="t1">texture to be drawn on</param>
    /// <param name="t2">texture including keyed area</param>
    /// <param name="key">key color</param>
    /// <param name="c">new color to be drawn</param>
    /// <returns>t1 with color c at keyed area</returns>
    public static Texture2D SetTexturesKey(Texture2D t1, Texture2D t2, Color key, Color c)
    {
        if (t1.width != t2.width || t1.height != t2.height)
        {
            throw new ArgumentException("AddTextures: Sizes of texture do not match.");
        }

        Texture2D addtex = new Texture2D(t1.width, t1.height, TextureFormat.RGB24, false);
        for (int x = 0; x < t1.width; x++)
        {
            for (int y = 0; y < t1.height; y++)
            {
                if (t2.GetPixel(x, y) == key)
                {
                    addtex.SetPixel(x, y, c);
                }
                else
                {
                    addtex.SetPixel(x, y, t1.GetPixel(x, y));
                }

            }
        }
        return addtex;
    }

    /// <summary>
    /// Generate Weights to fuse two textures based on key area based on distance to keyed area
    /// Idea: a texture as a higher weight the closer it is to a keyed area
    /// Approach similar to splatmapping
    /// Fast but coarse function, uses line segments used to draw sketch for distance calculation
    /// </summary>
    /// <param name="sketch">texture including the sketch as keyed area</param>
    /// <param name="keyColor">key color</param>
    /// <param name="lineSegments">line segments in sketch, definition through successive pairs of Vector2s</param>
    /// <returns>2d array with weights</returns>
    public static float[,] GenerateHeightmapWeightsCoarse(Texture2D sketch, Color keyColor, List<Vector2> lineSegments)
    {
        float[,] weights = new float[sketch.width, sketch.height];
        float maxDist = (sketch.width - 1) * 0.75f;

        for (int x = 0; x < sketch.height; x++)
        {
            for (int y = 0; y < sketch.width; y++)
            {
                //if (sketch.GetPixel(x, y) == keyColor) continue;

                float minDist = sketch.width + sketch.height; // initially an unrealistic high number
                int minDistIndex = -1; //index of point in linesegment with lowest distance

                // calcualte distance to all points of line segments, keep minimum
                for (int i = 0; i < lineSegments.Count; i++)
                {
                    //OPTIONAL prevent duplicates in the first place
                    // skip duplicate entries in line segment list
                    // process only: first, last and even numbered indizes
                    if (i != 0 && i != lineSegments.Count - 1)
                    {
                        if (i % 2 != 0) continue;
                    }

                    float tmp = Room.EuclideanDistance(x, y, (int)lineSegments[i].x, (int)lineSegments[i].y);
                    if (tmp < minDist)
                    {
                        minDist = tmp;
                        minDistIndex = i;
                    }
                }

                weights[x, y] = Mathf.Max(0.0f, (1 - (minDist / maxDist))); // capped at 0.0 to prevent flat plateaus
            }
        }

        return weights;
    }

    /// <summary>
    /// Generate Weights to fuse two textures based on key area based on distance to keyed area
    /// Idea: a texture as a higher weight the closer it is to a keyed area
    /// Approach similar to splatmapping
    /// Smooth but slow version, uses pixels with keycolor for distance calculation
    /// </summary>
    /// <param name="sketch">texture including the sketch as keyed area</param>
    /// <param name="keyColor">key color</param>
    /// <returns>2d array with weights</returns>
    public static float[,] GenerateHeightmapWeightsSmooth(Texture2D sketch, Color keyColor)
    {
        float[,] weights = new float[sketch.width, sketch.height];
        float maxDist = (sketch.width - 1) * 0.15f;
        List<Vector2> linePoints = new List<Vector2>();

        for (int x = 0; x < sketch.width; x++)
        {
            for (int y = 0; y < sketch.height; y++)
            {
                if (sketch.GetPixel(x, y) == keyColor) linePoints.Add(new Vector2(x, y));
            }
        }

        for (int x = 0; x < sketch.width; x++)
        {
            for (int y = 0; y < sketch.height; y++)
            {
                float minDist = sketch.width + sketch.height; // initially an unrealistic high number
                int minDistIndex = -1; //index of point in linesegment with lowest distance

                // calcualte distance to all points of line segments, keep minimum
                for (int i = 0; i < linePoints.Count; i++)
                {
                    float tmp = Room.EuclideanDistance(new Vector2(x, y), linePoints[i]);
                    if (tmp < minDist)
                    {
                        minDist = tmp;
                        minDistIndex = i;
                    }
                }

                weights[x, y] = Mathf.Max(0.0f, (1 - (minDist / maxDist))); // capped at 0.0 to prevent flat plateaus
            }
        }

        return weights;
    }

    /// <summary>
    /// Print ShapeGrammar onto a 2D texture
    /// </summary>
    /// <param name="lineSegments">line segments specifying sketch as successive pairs of Vector2s</param>
    /// <param name="width">Width of output texture</param>
    /// <param name="height">height of output texture</param>
    /// <param name="c">Color in which the sketch will be drawn</param>
    /// <returns>Texture2D object with black lines representing the grammar</returns>
    public static Texture2D PrintSketch(List<Vector2> lineSegments, int width, int height, Color c)
    {
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);
        tex = Generator.clearTexture(tex, Color.white);

        for (int i = 0; i < lineSegments.Count; i += 2) // i+=2 because always two successive vectors define a line segments. Its redundant, but it works
        {
            Generator.DrawLine(tex, (int)lineSegments[i].x, (int)lineSegments[i].y, (int)lineSegments[i + 1].x, (int)lineSegments[i + 1].y, c);
        }

        return tex;
    }

    /// <summary>
    /// widen sketch by tile width calculated by width/height of base texture
    /// </summary>
    /// <param name="sketchTexture">texture with 1px wide sketch</param>
    /// <param name="color">Color to be drawn with and specifying sketch</param>
    /// <returns></returns>
    public static Texture2D CreateWideSketch(Texture2D sketchTexture, Color color)
    {
        Texture2D sketchMap = new Texture2D(sketchTexture.width, sketchTexture.height, TextureFormat.RGB24, false);
        int sketchTileWidth = Mathf.FloorToInt(sketchTexture.width / 20);
        int sketchTileHeight = Mathf.FloorToInt(sketchTexture.height / 20);

        Color[] c = Generator.GenerateSingleColorArray(sketchTileWidth * sketchTileHeight, color);

#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("sketch.png", sketchTexture.EncodeToPNG());
#endif

        for (int x = 0; x < sketchTexture.width; x++)
        {
            for (int y = 0; y < sketchTexture.height; y++)
            {
                if (sketchTexture.GetPixel(x, y) != Color.white)
                {
                    // place tile
                    int x1 = Mathf.FloorToInt(x - (sketchTileWidth / 2));

                    // prevent going out of bounds
                    if (x1 < 0) x1 = 0;
                    if (sketchTileWidth + x1 > sketchTexture.width)
                    {
                        sketchTileWidth = sketchTileWidth - ((x1 + sketchTileWidth) - sketchTexture.width);
                    }

                    int y1 = Mathf.FloorToInt(y - (sketchTileHeight / 2));
                    if (y1 < 0) y1 = 0;
                    if (sketchTileHeight + y1 > sketchTexture.height)
                    {
                        sketchTileHeight = sketchTileHeight - ((y1 + sketchTileHeight) - sketchTexture.height);
                    }

                    sketchMap.SetPixels(x1, y1, sketchTileWidth, sketchTileHeight, c);

                    // for better performance (not paint same pixels multiple times), but does not work properly
                    //y += sketchTileHeight/2;
                    //x += sketchTileWidth / 2;
                    //continue;       
                }
            }
        }
#if UNITY_EDITOR && !UNITY_WEBPLAYER
        File.WriteAllBytes("sketch_wide.png", sketchMap.EncodeToPNG());
#endif

        return sketchMap;
    }
}
