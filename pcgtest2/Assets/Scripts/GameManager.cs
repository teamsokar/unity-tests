﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GameManager : MonoBehaviour {
    public LSystem.difficulty difficulty;


    public GUIStyle m_infotextStyle;
    private string m_infotext;
    public string Infotext
    {
        get { return m_infotext; }
        set { m_infotext = value; }
    }
    
    //private static Player m_player; // Player character

    private Vector3 m_lastPositionOnLandscape;
    public Vector3 LastPositionOnLandscape
    {
        get { return m_lastPositionOnLandscape; }
        set { m_lastPositionOnLandscape = value; }
    }

    private LSystem lsys;

    private static MissionGraph landscapeMission = null;
    public static MissionGraph LandscapeMission
    {
        get { return GameManager.landscapeMission; }
        //set { GameManager.landscapeMission = value; }
    }

    private MissionGraph mg;
    public MissionGraph Mg
    {
        get { return mg; }
        set { mg = value; }
    }
    private Texture2D sketch;
    public Texture2D Sketch
    {
        get { return sketch; }
        set { sketch = value; }
    }
    private List<Vector2> sketchLineSegments;

	/// <summary>
	/// Initialization
	/// </summary>
	void Awake () {
        //m_player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        lsys = new LSystem();
        if (Application.loadedLevelName == "Landscape" && landscapeMission == null)
        {
            lsys.Difficulty = difficulty;
            string m = lsys.StartProduction(true, true);
            landscapeMission = new MissionGraph(m);
            Debug.Log("GameManager: generated Landscape Mission: " + m);
        }

        GenerateMission();

        DontDestroyOnLoad(gameObject);
	}

    /// <summary>
    /// Starts mission generation process
    /// </summary>
    public void GenerateMission()
    {
        string missions = "";
        switch (Application.loadedLevelName)
        {
            case "Landscape":
                {
                    lsys.Difficulty = difficulty;
                    //missions = lsys.StartProduction(true, true);
                    break;
                }
            case "Cave":
                {
                    lsys.Difficulty = difficulty;
                    missions = lsys.StartProduction(true, false);
                    break;
                }
            case "Dungeon":
                {
                    lsys.Difficulty = difficulty;
                    missions = lsys.StartProduction(true, false);
                    break;
                }
            default:
                {
                    throw new System.ArgumentException("GameManager: unknown Scene loaded");
                }
        }
        mg = new MissionGraph(missions);
        Debug.Log("GameManager: generated Mission: " + missions);
    }
	
    /// <summary>
    /// Standard callback function for Unity to display a GUI
    /// </summary>
    void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 100, 20), "Main Menu"))
        {
            Application.LoadLevel(0);
        }
        //MissionDisplay();
        GUI.Label(new Rect(0, 0, Screen.width, 20), m_infotext, m_infotextStyle);
    }

    /// <summary>
    /// Draws a simple GUI of the current missions on screen
    /// </summary>
    public void MissionDisplay()
    {
        GUI.Label(new Rect(Screen.width - 80, 0, 80, 20), "Missions:");
        List<MissionNode> nodes;
        if (Application.loadedLevelName != "Landscape")
        {
            nodes = mg.GetNodes();
        }
        else
        {
            nodes = landscapeMission.GetNodes();
        }
        
        int j = 0;
        for (int i = 0; i < nodes.Count; i++)
        {
            if( !(nodes[i].Task == "S" || nodes[i].Task == "E") ){
                GUI.Label(new Rect(Screen.width - 80, 20 + 20 * j, 80, 20), nodes[i].Task + " " + nodes[i].Completed);
                j++;
            }
            
        }
    }

    /// <summary>
    /// Generate positions of objectives for mission
    /// </summary>
    /// <param name="width">Width of noise texture</param>
    /// <param name="height">Height of noise texture</param>
    public void GenerateMissionPositions(int width, int height)
    {

        if (Application.loadedLevelName != "Landscape")
        {
            this.mg.calculateSketchPoints(width, height);
        }
        else
        {
            GameManager.LandscapeMission.calculateSketchPoints(width, height);
        }

        if (Application.loadedLevelName != "Dungeon")
        {
            if (Application.loadedLevelName == "Cave")
            {
                this.sketchLineSegments = mg.GenerateLineSegments();
            }
            else
            {
                this.sketchLineSegments = GameManager.LandscapeMission.GenerateLineSegments();
            }

            this.sketch = Generator.PrintSketch(this.sketchLineSegments, width, height, Color.green);
        #if UNITY_EDITOR && !UNITY_WEBPLAYER
            File.WriteAllBytes(Application.loadedLevelName + "_sketch.png", this.sketch.EncodeToPNG());
        #endif
        }  
    }

    /// <summary>
    /// Loads another Level (Scene)
    /// Displays loading screen
    /// </summary>
    /// <param name="scene">Name of the scene</param>
    public void InitiateLevelTransition(string scene, Vector3 pos)
    {
        switch (scene)
        {
            // transitioning from dungeon/cave to landscape
            case "Landscape":
                {
                    break;
                }
            // transitioning from landscape to dungeon/cave
            case "Dungeon":
            case "Cave":
                {
                    this.LastPositionOnLandscape = pos;
                    break;
                }
            default: throw new System.ArgumentException("GameManager::InitiateLevelTransition: unknown scene requested to load");
        }

        Debug.Log("GameManager: initiate Level transition to: " + scene);
        LoadingScreen.show();
        Application.LoadLevel(scene);
    }

    /// <summary>
    /// Marks an objective as completed
    /// </summary>
    /// <param name="objective">Name of objective</param>
    public void ObjectiveCompleted(string objective, bool landscapeMission = false)
    {
        string s;
        List<MissionNode> nodes;
        if (landscapeMission)
        {
            nodes = LandscapeMission.GetNodes();
        }
        else
        {
            nodes = mg.GetNodes();
        }

        if (lsys.Constants.TryGetValue(objective, out s))
        {
            for (int i = 0; i < nodes.Count; i++)
            {
                if (nodes[i].Task == s)
                {
                    nodes[i].Completed = true;
                    Debug.Log("GameManager: Objective _" + nodes[i].Task + "_ completed");
                    this.Infotext = "Objective " + nodes[i].Task + " completed";
                }
            }
        }
        else
        {
            throw new System.ArgumentException("GameManager: Objective to complete not found: " + objective);
        }
    }

    /// <summary>
    /// Marks an objective als completed
    /// </summary>
    /// <param name="obj">Objective object to be completed</param>
    public void ObjectiveCompleted(Objective obj, bool landscapeMission = false)
    {
        string s;
        List<MissionNode> nodes;
        if (landscapeMission)
        {
            nodes = LandscapeMission.GetNodes();
        }
        else
        {
            nodes = mg.GetNodes();
        }

        if (lsys.Constants.TryGetValue(obj.name.Substring(0, obj.name.Length - 7), out s))
        {
            for(int i = 0; i < nodes.Count; i++){
                if(nodes[i].Task == s && nodes[i].Completed == false){
                    nodes[i].Completed = true;
                    Debug.Log("GameManager: Objective " + nodes[i].Task + " completed");
                    this.Infotext = "Objective " + nodes[i].Task + " completed";
                    return;
                }
            }
        }
        else
        {
            throw new System.ArgumentException("GameManager: Objective to complete not found" + obj.name);
        }
    }

    /// <summary>
    /// Check if prerequierment objectives of current objective are completed
    /// </summary>
    /// <param name="obj">Objective object to be completed</param>
    /// <returns>false if an uncompleted prerequiered objective is found; otherwise true</returns>
    public bool PrerequierementObjectivesCompleted(Objective obj)
    {
        string s;
        List<MissionNode> nodes = mg.GetNodes();
        if (lsys.Constants.TryGetValue(obj.name.Substring(0, obj.name.Length - 7), out s))
        {
            //Debug.Log("GameManager PrereqObj Name: " + obj.name);
            string o = obj.name.Substring(0, 1);
            switch (o)
            {
                case "C": // chest
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            if (nodes[i].Task == "K")
                            {
                                if (!nodes[i].Completed) return false;
                            }
                        }
                        break;
                    }

                case "B": // kill boss
                    {
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            if (nodes[i].Task == "E")
                            {
                                if (!nodes[i].Completed) return false;
                            }
                        }
                        break;
                    }

                default:
                    {
                        throw new System.ArgumentException("GameManager PrerequierementObjectivesCompleted: unknown objective requested");
                    }
            }

        }

        return true;
    }

    /*
    /// <summary>
    /// Move player character to a different position
    /// Uses function in Player-class
    /// </summary>
    /// <param name="pos">position</param>
    public void MovePlayer(Vector3 pos)
    {
        //player.SendMessage("MovePlayer", pos);
        m_player.MovePlayer(pos);
    }
     * */
    
}
