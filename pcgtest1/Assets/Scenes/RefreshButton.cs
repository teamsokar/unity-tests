﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RefreshButton : MonoBehaviour {

	Text text;

	void Awake(){
		text = GetComponent<Text>();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnButtonPres(){
		gameObject.SendMessage("Generate");
	}
}
