﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// Code from: http://blog.apiad.net/procedural-terrain-generation-in-unity-3d-part-i/

public class TerrainGenerator : MonoBehaviour {

    public int Samples = 200;
    public float Width = 200;
    public int PerlinLevels = 3;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void Generate()
    {
        float edge = Width / (Samples - 1);

        Vector3 [] vertices = new Vector3[Samples * Samples];
        Vector2 [] uv = new Vector2[Samples * Samples];

        for (int i = 0, p = 0; i < Samples; i++)
        {
            for (int j = 0; j < Samples; j++)
            {
                Vector3 center = new Vector3(i * edge, 0, j * edge);
                float h = SampleHeight(center.x, center.y);
                center.y = h;
                vertices[p] = center;

                uv[p++] = new Vector2(i / (Samples - 1f), j / (Samples - 1f));

            }
        }
    }

    private float SampleHeight(float x, float y)
    {
        return Mathf.PerlinNoise(x, y);
    }

    private float SamplePerlin(float x, float y)
    {
        float width = Width / 10f;
        float result = 0;

        for (int i = 0; i < PerlinLevels; i++)
        {
            result += (Mathf.PerlinNoise(x / width, y / width)) * width / 2;
            width /= 10;
        }

        return result;
    }

    private float SampleGaussian(float x, float mu, float sigma)
    {
        float d = (x - mu);
        return Mathf.Exp(-d * d / (sigma * sigma));
    }
}

public class Island : MonoBehaviour
{

    private float Radius;
    private Vector3 Center;

    public Island(Vector3 c, float r)
    {
        this.Center = c;
        this.Radius = r;
    }
    private Island[] GenerateIslands(int count, float size, float density, float separation)
    {
        List<Island> islands = new List<Island>();

        for (int i = 0; i < count; i++)
        {
            for (int j = 0; j < count; j++)
            {
                if (!RandomBernoulli(density))
                {
                    continue;
                }

                float x = i * size * RandomUniform(0, size - size * separation) - size * count / 2;
                float z = j * size * RandomUniform(0, size - size * separation) - size * count / 2;
                Island island = new Island(new Vector3(x, 0, z), 0);
                islands.Add(island);
            }
        }

        bool[] cannotGrow = new bool[islands.Count];
        int canGrow = islands.Count;

        while (canGrow > 0)
        {
            for (int i = 0; i < islands.Count; i++)
            {
                var child = islands[i];

                if (cannotGrow[i])
                {
                    continue;
                }

                for (int j = 0; j < islands.Count; j++)
                {
                    if (i == j)
                    {
                        continue;
                    }

                    var other = islands[j];

                    if (Vector3.Distance(child.Center, other.Center) - size * separation < (child.Radius + other.Radius))
                    {
                        cannotGrow[i] = true;
                        canGrow--;
                        break;
                    }
                }
                if (!cannotGrow[i])
                {
                    child.Radius += size * 0.1f;
                }
            }
        }

        return islands.ToArray();
    }

    private float RandomUniform(int p1, float p2)
    {
        //throw new System.NotImplementedException();
        if (p1 > p2)
        {
            return Random.Range(p1, p2);
        }
        else
        {
            return Random.Range(p2, p1);
        }
    }

    private bool RandomBernoulli(float density)
    {
        //throw new System.NotImplementedException();
        int trials = 1;
        int sucess = 1;

        return true;
    }

    //public static int Factorial(this int x)
    public int Factorial(int x)
    {
        //return x <= 1 ? 1 : x * Factorial(x - 1);
        return 0;
    }
}
