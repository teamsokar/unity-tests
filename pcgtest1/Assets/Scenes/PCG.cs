﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using System;
using System.IO;
using System.Linq;

using LibNoise;
using LibNoise.Generator;
using LibNoise.Operator;

//using TextureFilter;

public class PCG : MonoBehaviour {

    // Enums that show up in the Unity-Dev-UI for testing purposes
    public enum PCGMode{ rawFractal, cave, overworld };
    public bool showNoiseTexture = false;
    public PCGMode pcgMode = PCGMode.rawFractal;

    public enum CaveMode { lakes, walls, both }; // how boundaries of the cave are created: lakes are lower than the level-plane, walls higher. Both randomly decides between the two, but is currently deactivated due to performance issues -20150111
    public CaveMode caveMode = CaveMode.walls;
    public enum PCGGenerator { perlinBasic, libnoise };
    public PCGGenerator pcgGenerator;

    //public String debugFilename = "noiseColors.txt"; // outputs gray values to file for debug purposes

    private Texture2D noiseTexture;
    public Material material;
    public Color color;
    public Vector3 meshSize = new Vector3(200, 30, 200);
    public bool continuousUpdate = false; // generates a new noiseTexture and Mesh every frame

    private PerlinBasic perlinBasic; // from unity pcg example
    private FractalNoise fractal;   // from Libnoise.unity

    // Noise Parameters
    public bool gray = true;
    public int noiseWidth = 128; // size of noise texture
    public int noiseHeight = 128;

    // Parameters for NoiseMap Generation

    // parameters used by both
    public float octaves = 8.379f; // Number of noise functions that are added up; min 1, for libNoise will be floored to integer

    // Unity PCG Example - Hybrid Multi Fracal
    // descrition found on: http://innerworld.sourceforge.net/IwSrcHybridMFractalNoise.html
    public float lacunarity = 6.18f;    // det: Lückenhaftigkeit; gap between successive frequencies
    public float h = 0.69f;             // Fractal increment paramenter; 0.0f - 2.0f
    public float offset = 0.75f;    // change between iterations
    public float scale = 0.09f;     // scaling of noise texture onto mesh
    public float offsetPos = 0.0f; // current offset, produces cascading-like-shapes

    // for Cave generator
    public float caveThreshold = 0.25f; //~0.2 for PerlinBasic, ~0,5 for libNoise (higher amplitude)

    // for libnoise.unity
    private Noise2D noiseMap = null; //has its own format for a noiseMap, Unity-example uses a Texture2D-object
    public float zoom = 1f; // scaling of noise texture onto mesh - similar to scale in pcg example

    // for Texture filter
    public enum FilterMode { median, gaussian };
    public FilterMode filterMode = FilterMode.median;
    public int medianFilterMatrixSize = 3; //usually uneven numbers from 3 to 11
    public int filterOffset = 0;
    public int filterFactor = 1;

    // filter matrizes for convolution filter
    // generation: http://www.embege.com/gauss/
    private float[,] gaussian3x3 = {
                                     {0.14676266317374237f, 0.24197072451914536f, 0.14676266317374237f}, 
                                     {0.24197072451914536f, 0.3989422804014327f, 0.24197072451914536f}, 
                                     {0.14676266317374237f, 0.24197072451914536f, 0.14676266317374237f}
                                 };

    private float[,] gaussian5x5 = {
                                       {0.0073068827452812644f, 0.03274717653776802f, 0.05399096651318985f, 0.03274717653776802f, 0.0073068827452812644f}, 
                                       {0.03274717653776802f, 0.14676266317374237f, 0.24197072451914536f, 0.14676266317374237f, 0.03274717653776802f}, 
                                       {0.05399096651318985f, 0.24197072451914536f, 0.3989422804014327f, 0.24197072451914536f, 0.05399096651318985f}, 
                                       {0.03274717653776802f, 0.14676266317374237f, 0.24197072451914536f, 0.14676266317374237f, 0.03274717653776802f}, 
                                       {0.0073068827452812644f, 0.03274717653776802f, 0.05399096651318985f, 0.03274717653776802f, 0.0073068827452812644f}
                                   };

	// Use this for initialization
	void Start () {
        noiseTexture = new Texture2D(noiseWidth, noiseHeight, TextureFormat.RGB24, false);

        // add mesh to GameObject
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();

        Generate();  // Generates MeshDetails (Vertices, UV-vectors, etc) and adds it to the mesh currently attached to the GameObject

        // generate mesh-collider
        gameObject.AddComponent<MeshCollider>();
	}
	
	// Update is called once per frame
	void Update () {
        if (continuousUpdate)
        {
            Generate();
            DestroyImmediate(gameObject.GetComponent<MeshCollider>()); // gets rid of old collider
            gameObject.AddComponent<MeshCollider>(); // creates new one - very calculation intensive, handle with care!
        }
	}

    void OnGUI()
    {
        if (Debug.isDebugBuild) // Generate Button in upper left corner only appears on debug builds
        {
            if (GUI.Button(new Rect(0, 0, 100, 20), "Generate"))
            {
                Generate();
                DestroyImmediate(gameObject.GetComponent<MeshCollider>());
                gameObject.AddComponent<MeshCollider>();
            }
        }
        else //controls for release builds to test
        // water gameobjects in separate gamemanager class? cannot be accessed they it is coded right now
        {
            if (GUI.Button(new Rect(0, 0, 200, 20), "Generate Cave with Walls"))
            {
                pcgMode = PCGMode.cave;
                caveMode = CaveMode.walls;
                Generate();
                DestroyImmediate(gameObject.GetComponent<MeshCollider>());
                gameObject.AddComponent<MeshCollider>();
                /*
                GameObject dayWater = GameObject.FindGameObjectWithTag("daywater");
                dayWater.SetActive(false);
                GameObject nightWater = GameObject.FindGameObjectWithTag("nightwater");
                nightWater.SetActive(false);
                 * */
            }

            if (GUI.Button(new Rect(0, 20, 200, 20), "Generate Cave with Lakes"))
            {
                pcgMode = PCGMode.cave;
                caveMode = CaveMode.lakes;
                Generate();
                DestroyImmediate(gameObject.GetComponent<MeshCollider>());
                gameObject.AddComponent<MeshCollider>();
                /*
                GameObject dayWater = GameObject.FindGameObjectWithTag("daywater");
                dayWater.SetActive(false);
                GameObject nightWater = GameObject.FindGameObjectWithTag("nightwater");
                nightWater.SetActive(true);
                 */
            }

            if (GUI.Button(new Rect(0, 40, 200, 20), "Generate Landscape"))
            {
                pcgMode = PCGMode.overworld;
                Generate();
                DestroyImmediate(gameObject.GetComponent<MeshCollider>());
                gameObject.AddComponent<MeshCollider>();
                /*
                GameObject dayWater = GameObject.FindGameObjectWithTag("daywater");
                dayWater.SetActive(true);
                GameObject nightWater = GameObject.FindGameObjectWithTag("nightwater");
                nightWater.SetActive(false);
                 * */
                
                //GameObject.Find("DaylightSimpleWater").SetActive(true);
                //GameObject.Find("Nighttime Simple Water").SetActive(false);
            }
        }       
    }

    void Generate()
    {
        noiseTexture = GenerateNoiseTexture();
        switch (pcgMode)
        {
            case PCGMode.rawFractal:
                GenerateHeightmapFromNoiseTexture();
                break;
            case PCGMode.cave:
                GenerateCaveMapFromNoiseTexture();
                GenerateHeightmapFromNoiseTexture();
                break;
            case PCGMode.overworld:
                GenerateLandscape();
                GenerateHeightmapFromNoiseTexture();
                break;
        }

        if (showNoiseTexture && Debug.isDebugBuild)
        {
            noiseTexture.Apply();
        }
    }

    private Texture2D GenerateNoiseTexture()
    {
        switch (pcgGenerator)
        {
            case PCGGenerator.perlinBasic:
                return GenerateNoiseTextureBasic();
            case PCGGenerator.libnoise:
                return GenerateNoiseTextureLibNoise();
        }
        throw new System.ArgumentException("unknown PCGGenerator selected");
        //return null;
    }

    // taken straight from Unity-PCG-example
    Texture2D GenerateNoiseTextureBasic()
    {
        Texture2D noiseTexture = new Texture2D(noiseWidth, noiseHeight, TextureFormat.RGB24, false);

        if (perlinBasic == null)
        {
            perlinBasic = new PerlinBasic();
        }

        fractal = new FractalNoise(h, lacunarity, octaves, perlinBasic);

        for (int x = 0; x < noiseWidth; x++)
        {
            for (int y = 0; y < noiseHeight; y++ )
            {
                if (gray)
                {
                    float value = fractal.HybridMultifractal(x * scale + Time.time, y * scale + Time.time, offset);
                    noiseTexture.SetPixel(x, y, new Color(value, value, value, value));
                }
                else
                {
                    offsetPos = Time.time;
                    float valueX = fractal.HybridMultifractal(x * scale + offsetPos * 0.6f, y * scale + offsetPos * 0.6f, offset);
                    float valueY = fractal.HybridMultifractal(x * scale + 161.7f + offsetPos, y * scale + 161.7f + offsetPos * 0.3f, offset);
                    float valueZ = fractal.HybridMultifractal(x * scale + 591.1f + offsetPos, y * scale + 591.1f + offsetPos * 0.1f, offset);
                    noiseTexture.SetPixel(x, y, new Color(valueX, valueY, valueZ, 1));                        
                }
            }
        }

        if (Debug.isDebugBuild)
        {
            File.WriteAllBytes("noisetexture_aftergeneration_perlinbasic.png", noiseTexture.EncodeToPNG());
        }

        return noiseTexture;
    }

    //taken from example; does always create the same noiseTexture for whatever reason
    Texture2D GenerateNoiseTextureLibNoise()
    {
        ModuleBase moduleBase;
        Perlin perlin = new Perlin();
        //Perlin perlin = new Perlin(1.0f, lacunarity, 0f, octaves, Time.time, QualityMode.Medium);
        perlin.OctaveCount = (int)Math.Floor(octaves);
        moduleBase = perlin;

        noiseMap = new Noise2D(noiseWidth, noiseHeight, moduleBase);

        //offset = Time.time;
        
        noiseMap.GeneratePlanar(
            offset + -1 * 1 / zoom, 
            offset + offset + 1 * 1 / zoom,
            offset + -1 * 1 / zoom,
            offset + 1 * 1 / zoom,
            true);

        if (Debug.isDebugBuild)
        {
            File.WriteAllBytes("noisetexture_aftergeneration_libnoise.png", noiseMap.GetTexture(GradientPresets.Grayscale).EncodeToPNG());
        }

        return noiseMap.GetTexture(GradientPresets.Grayscale);
        
    }

    // constructs mesh based on values in noiseTextures. Values affect mainly the z-coordinate of the vertices
    void GenerateHeightmapFromNoiseTexture()
    {

        if (!showNoiseTexture)
        {
            renderer.material.mainTexture = null;
            if (material != null)
            {
                renderer.material = material;
            }
            else
            {
                renderer.material.color = color;
            }
        }
        else
        {
            renderer.material.mainTexture = noiseTexture;
        }

        Mesh mesh = GetComponent<MeshFilter>().mesh;
        int width = Mathf.Min(noiseTexture.width, 255);
        int height = Mathf.Min(noiseTexture.height, 255);
        int x = 0, y = 0;

        Vector3[] vertices = new Vector3[width * height];
        Vector2[] uv = new Vector2[height * width];
        Vector4[] tangents = new Vector4[height * width];

        Vector2 uvScale = new Vector2(1.0f / (width - 1), 1.0f / (height - 1));
        Vector3 sizeScale = new Vector3(meshSize.x / (width - 1), meshSize.y, meshSize.z / (height - 1));

        for (x = 0; x < width; x++)
        {
            for (y = 0; y < height; y++)
            {
                float pixelHeight = noiseTexture.GetPixel(x, y).grayscale;
                Vector3 vertex = new Vector3(x, pixelHeight, y);
                vertices[y * width + x] = Vector3.Scale(sizeScale, vertex);
                uv[y * width + x] = Vector2.Scale(new Vector2(x, y), uvScale);

                // Calculate tangent vector, used with bumpmap shaders on mesh
                Vector3 vertexL = new Vector3(x - 1, noiseTexture.GetPixel(x - 1, y).grayscale);
                Vector3 vertexR = new Vector3(x + 1, noiseTexture.GetPixel(x + 1, y).grayscale);
                Vector3 tan = Vector3.Scale(sizeScale, vertexR - vertexL).normalized;
                tangents[y * width + x] = new Vector4(tan.x, tan.y, tan.z, -1.0f);
            }
        }

        mesh.vertices = vertices;
        mesh.uv = uv;

        // build triangle indices
        int[] triangles = new int[(height - 1) * (width - 1) * 6];
        int index = 0;

        for (x = 0; x < width - 1; x++)
        {
            for (y = 0; y < height - 1; y++)
            {
                triangles[index++] = (y * width) + x;
                triangles[index++] = ((y + 1) * width) + x;
                triangles[index++] = (y * width) + x + 1;

                triangles[index++] = ((y + 1) * width) + x;
                triangles[index++] = ((y + 1) * width) + x + 1;
                triangles[index++] = (y * width) + x + 1;
            }
        }

        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        mesh.tangents = tangents;
    }

    private void GenerateCaveMapFromNoiseTexture()
    {
        int width = noiseTexture.width;
        int height = noiseTexture.height;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {

                // build that wall
                if (x == 0 || y == 0 || x == width - 1 || y == height - 1)
                {
                    noiseTexture.SetPixel(x, y, Color.white);
                    continue;
                }
                
                float c = noiseTexture.GetPixel(x, y).grayscale;

                //System.IO.File.AppendAllText(debugFilename, c.ToString() + "\n"); 
                if (c < caveThreshold)
                {
                    switch (caveMode)
                    {
                        case CaveMode.lakes: noiseTexture.SetPixel(x, y, Color.black); break;
                        case CaveMode.walls: noiseTexture.SetPixel(x, y, Color.white); break;
                        case CaveMode.both: /*CreateLakeWallsInCave(Color.gray, 0, 0); */ Debug.Log("CaveMode.both not implemented"); break; // deactivated, time to generate goes from a second to "I got coffee and still isn't finished, I'm gonna kill the process"
                    }
                    //noiseTexture.SetPixel(x, y, Color.red); // wall or lake, not yet processed
                }
                else
                {
                    noiseTexture.SetPixel(x, y, Color.gray); // floor
                }                
            }
        }

        if (Debug.isDebugBuild)
        {
            File.WriteAllBytes("noisetexture_aftercave.png", noiseTexture.EncodeToPNG());
        }
    }

    private void CreateLakeWallsInCave(Color mode, int x, int y)
    {
           // error? does over every pixel multiple times - times the width and height of the texture? that would be insane... #2015-01-12
        if (x >= noiseTexture.width || y >= noiseTexture.height)
        {
            return;
        }

        if (noiseTexture.GetPixel(x, y) == Color.red)
        {
            if (mode == Color.gray)
            {
                if (UnityEngine.Random.value > 0.5)
                {
                    mode = Color.black;
                }
                else
                {
                    mode = Color.white;
                }
            }

            noiseTexture.SetPixel(x, y, mode);
        }

        if (noiseTexture.GetPixel(x, y) == Color.gray)
        {
            mode = Color.gray;
        }
        
        CreateLakeWallsInCave(mode, x + 1, y);
        CreateLakeWallsInCave(mode, x, y + 1);
    }

    private void GenerateLandscape()
    {
        if (filterMode == FilterMode.median)
        {
            noiseTexture = MedianFilter(noiseTexture, medianFilterMatrixSize, medianFilterMatrixSize);
        }
        else if (filterMode == FilterMode.gaussian)
        {
            noiseTexture = ConvolutionFilter(noiseTexture, gaussian5x5, filterFactor, filterOffset);
        }
        

        if (Debug.isDebugBuild)
        {
            File.WriteAllBytes("noisetexture_afterMedianFilter.png", noiseTexture.EncodeToPNG());
        }

        for (int x = 0; x < noiseTexture.width; x++)
        {
            for (int y = 0; y < noiseTexture.height; y++)
            {
                if (x == 0 || y == 0 || x == noiseTexture.width-1 || y == noiseTexture.height-1)
                {
                    noiseTexture.SetPixel(x, y, Color.black);
                    continue;
                }

                // create slope at border
                int borderWidth = (int)Mathf.Floor(noiseTexture.width * 0.05f);
                int borderHeight = (int)Mathf.Floor(noiseTexture.height * 0.05f);
                float max = 1f;
                float t = 0.0f;

                max = noiseTexture.GetPixel(x, borderWidth).grayscale;
                t = max / borderWidth; //interpolation factor
                for (int i = 0; i < borderWidth; i++)
                {
                    noiseTexture.SetPixel(x, i, new Color(i * t, i * t, i * t));
                }

                max = noiseTexture.GetPixel(x, noiseTexture.width - borderWidth).grayscale;
                t = max / borderWidth;
                for (int i = borderWidth; i > 0; i--)
                {
                    noiseTexture.SetPixel(x, noiseTexture.width - i, new Color(i * t, i * t, i * t));
                }

                max = noiseTexture.GetPixel(borderHeight, y).grayscale;
                t = max / borderHeight; //interpolation factor
                for (int i = 0; i < borderHeight; i++)
                {
                    noiseTexture.SetPixel(i, y, new Color(i * t, i * t, i * t));
                }

                max = noiseTexture.GetPixel(noiseTexture.height - borderHeight, y).grayscale;
                t = max / borderHeight;
                for (int i = borderHeight; i > 0; i--)
                {
                    noiseTexture.SetPixel(noiseTexture.height - i, y, new Color(i * t, i * t, i * t));
                }
            }
        }

        if (Debug.isDebugBuild)
        {
            File.WriteAllBytes("noisetexture_afterlerp.png", noiseTexture.EncodeToPNG());
        }
    }

    // adapted from http://en.wikipedia.org/wiki/Median_filter#2D_median_filter_pseudo_code
    private Texture2D MedianFilter(Texture2D sourceTexture, int windowWidth, int windowHeight)
    {
        int edgeX = (int)Mathf.Floor((windowWidth / 2));
        int edgeY = (int)Mathf.Floor((windowHeight / 2));
        Texture2D resultTexture = new Texture2D(sourceTexture.width, sourceTexture.height, TextureFormat.RGB24, false);
        List<Color> window = new List<Color>();

        for (int x = edgeX; x < sourceTexture.width - edgeX; x++)
        {
            for (int y = edgeY; y < sourceTexture.height - edgeY; y++)
            {
                window.Clear();
                for (int fx = 0; fx < windowWidth; fx++)
                {
                    for (int fy = 0; fy < windowHeight; fy++)
                    {
                        window.Add(sourceTexture.GetPixel(x+fx-edgeX, y+fy-edgeY));
                    }
                }
                window = window.OrderBy(Color => Color.grayscale).ToList();
                resultTexture.SetPixel(x, y, window[(int)((windowWidth * windowHeight) / 2)]);
            }
        }

        return resultTexture;
    }

    // adapted from: http://studentguru.gr/b/jupiter/archive/2009/10/14/creating-an-image-processing-library-with-c-part-1
    private Texture2D ConvolutionFilter(Texture2D sourceTexture, float[,] filterMatrix, float factor = 1, int filterOffset = 0){
        if (factor == 0)
        {
            throw new ArgumentException("Factor is not allowed to be 0");
        }

        Texture2D resultTexture = new Texture2D(sourceTexture.width, sourceTexture.height, TextureFormat.RGB24, false);

        int s = (filterMatrix.GetLength(0) + filterMatrix.GetLength(1)) / 2;
        float r, b, g = 0.0f;
        Color tempPix;

        for (int x = s; x < sourceTexture.width; x++)
        {
            for (int y = s; y < sourceTexture.height; y++)
            {
                r = b = g = 0.0f;
                for (int filterX = 0; filterX < filterMatrix.GetLength(1); filterX++)
                {
                    for (int filterY = 0; filterY < filterMatrix.GetLength(0); filterY++)
                    {
                        tempPix = sourceTexture.GetPixel(x + filterX - s, y + filterY - s);
                        r += filterMatrix[filterX, filterY] * tempPix.r;
                        g += filterMatrix[filterX, filterY] * tempPix.g;
                        b += filterMatrix[filterX, filterY] * tempPix.b;

                    }
                }

                r = Mathf.Min(Mathf.Max((r / factor) + filterOffset, 0.0f), 1.0f);
                g = Mathf.Min(Mathf.Max((g / factor) + filterOffset, 0.0f), 1.0f);
                b = Mathf.Min(Mathf.Max((b / factor) + filterOffset, 0.0f), 1.0f);

                resultTexture.SetPixel(x, y, new Color(r, g, b));
            }
        }


        return resultTexture;
    }
}
