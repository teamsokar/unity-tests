﻿using UnityEngine;
using System.Collections;

using System;
using System.IO;

public class PCG : MonoBehaviour {

    private Texture2D noiseMap;
    public Material material;
    public Color color;
    public Vector3 size = new Vector3(200, 30, 200);
    public bool continuousUpdate = false;

    private Perlin perlin;
    private FractalNoise fractal;

    // Noise Parameters
    public bool gray = true;
    public int width = 128;
    public int height = 128;

    // Parameters for NoiseMap Generation
    // descrition found on: http://innerworld.sourceforge.net/IwSrcHybridMFractalNoise.html
    public float lacunarity = 6.18f;    // det: LÜckenhaftigkeit; gap between successfive frequencies
    public float h = 0.69f;             // Fractal increment paramenter; 0.0f - 2.0f
    public float octaves = 8.379f; // Number of noise functions that are added up
    public float offset = 0.75f;
    public float scale = 0.09f;
    public float offsetPos = 0.0f;

	// Use this for initialization
	void Start () {
        gameObject.AddComponent<MeshFilter>();
        gameObject.AddComponent<MeshRenderer>();

        //GenerateHeightmapFromNoiseTexture(GenerateNoise());
        GenerateHeightmapFromNoiseTexture(GenerateCaveMapFromNoiseTexture(GenerateNoise()));
	}
	
	// Update is called once per frame
	void Update () {
        if (continuousUpdate)
        {
            //GenerateHeightmapFromNoiseTexture(GenerateNoise());
            GenerateHeightmapFromNoiseTexture(GenerateCaveMapFromNoiseTexture(GenerateNoise()));
        }
	}

    void Generate()
    {

    }

    Texture2D GenerateNoise()
    {
        Texture2D noiseTexture = new Texture2D(width, height, TextureFormat.RGB24, false);

        if (perlin == null)
        {
            perlin = new Perlin();
        }

        fractal = new FractalNoise(h, lacunarity, octaves, perlin);

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++ )
            {
                if (gray)
                {
                    float value = fractal.HybridMultifractal(x * scale + Time.time, y * scale + Time.time, offset);
                    noiseTexture.SetPixel(x, y, new Color(value, value, value, value));
                }
                else
                {
                    offsetPos = Time.time;
                    float valueX = fractal.HybridMultifractal(x * scale + offsetPos * 0.6f, y * scale + offsetPos * 0.6f, offset);
                    float valueY = fractal.HybridMultifractal(x * scale + 161.7f + offsetPos, y * scale + 161.7f + offsetPos * 0.3f, offset);
                    float valueZ = fractal.HybridMultifractal(x * scale + 591.1f + offsetPos, y * scale + 591.1f + offsetPos * 0.1f, offset);
                    noiseTexture.SetPixel(x, y, new Color(valueX, valueY, valueZ, 1));                        
                }
            }
        }

        return noiseTexture;
    }

    void GenerateHeightmapFromNoiseTexture(Texture2D noiseTexture)
    {
        if (material != null)
        {
            renderer.material = material;
        }
        else
        {
            if (color != null)
            {
                renderer.material.color = color;
            }
            else
            {
                renderer.material.color = Color.white;
            }
            
        }

        Mesh mesh = GetComponent<MeshFilter>().mesh;
        int width = Mathf.Min(noiseTexture.width, 255);
        int height = Mathf.Min(noiseTexture.height, 255);
        int x = 0, y = 0;

        Vector3[] vertices = new Vector3[width * height];
        Vector2[] uv = new Vector2[height * width];
        Vector4[] tangents = new Vector4[height * width];

        Vector2 uvScale = new Vector2(1.0f / (width - 1), 1.0f / (height - 1));
        Vector3 sizeScale = new Vector3(size.x / (width - 1), size.y, size.z / (height - 1));

        for (x = 0; x < width; x++)
        {
            for (y = 0; y < height; y++)
            {
                float pixelHeight = noiseTexture.GetPixel(x, y).grayscale;
                Vector3 vertex = new Vector3(x, pixelHeight, y);
                vertices[y * width + x] = Vector3.Scale(sizeScale, vertex);
                uv[y * width + x] = Vector2.Scale(new Vector2(x, y), uvScale);

                // Calculate tangent vector, used with bumpmap shaders on mesh
                Vector3 vertexL = new Vector3(x - 1, noiseTexture.GetPixel(x - 1, y).grayscale);
                Vector3 vertexR = new Vector3(x + 1, noiseTexture.GetPixel(x + 1, y).grayscale);
                Vector3 tan = Vector3.Scale(sizeScale, vertexR - vertexL).normalized;
                tangents[y * width + x] = new Vector4(tan.x, tan.y, tan.z, -1.0f);
            }
        }

        mesh.vertices = vertices;
        mesh.uv = uv;

        // buld triangle indices
        int[] triangles = new int[(height - 1) * (width - 1) * 6];
        int index = 0;

        for (x = 0; x < width - 1; x++)
        {
            for (y = 0; y < height - 1; y++)
            {
                triangles[index++] = (y * width) + x;
                triangles[index++] = ((y + 1) * width) + x;
                triangles[index++] = (y * width) + x + 1;

                triangles[index++] = ((y + 1) * width) + x;
                triangles[index++] = ((y + 1) * width) + x + 1;
                triangles[index++] = (y * width) + x + 1;
            }
        }

        mesh.triangles = triangles;
        mesh.RecalculateNormals();
        mesh.tangents = tangents;
    }

    Texture2D GenerateCaveMapFromNoiseTexture(Texture2D noiseTexture)
    {

        
        int width = noiseTexture.width;
        int height = noiseTexture.height;
        Texture2D texture = new Texture2D(width, height, TextureFormat.RGB24, false);

        //String debugFilename = "noiseColors.txt";

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                /*
                Color c = noiseTexture.GetPixel(x, y);
                if (c.r > 0.5f && c.g > 0.5f && c.b > 0.5f)
                {
                    texture.SetPixel(x,y,Color.white);
                }
                else
                {
                    texture.SetPixel(x, y, Color.black);
                }
                 */

                float c = noiseTexture.GetPixel(x, y).grayscale;
                //System.IO.File.AppendAllText(debugFilename, c.ToString() + "\n");
                if (c < 0.25f)
                {
                    texture.SetPixel(x, y, Color.white);
                }
                else
                {
                    texture.SetPixel(x, y, Color.black);
                }
            }
        }

        //System.IO.File.AppendAllText(debugFilename, "\n\n\n");

        return texture;
    }
}
