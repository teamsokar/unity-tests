﻿using UnityEngine;
using System.Collections;

public class PCGHeightmap : MonoBehaviour {

	private Texture2D heightMap;
	public Material material;
	public Vector3 size = new Vector3(200, 30, 200);


	public bool gray = true;
	public int width = 128;
	public int height = 128;
	
	public float lacunarity = 6.18f;
	public float h = 0.69f;
	public float octaves = 8.379f;
	public float offset = 0.75f;
	public float scale = 0.09f;
	
	public float offsetPos = 0.0f;
	
	private Texture2D texture;
	private Perlin perlin;
	private FractalNoise fractal;

	// Use this for initialization
	void Start () {
		texture = new Texture2D(width, height, TextureFormat.RGB24, false);
		heightMap = Calculate();

		gameObject.AddComponent<MeshFilter>();
		gameObject.AddComponent<MeshRenderer>();
		GenerateHeightmap();
	}
	
	// Update is called once per frame
	void Update () {
		//heightMap = Calculate();
		//GenerateHeightmap();
	}

	void Generate(){
		heightMap = Calculate();
		GenerateHeightmap();
	}


	void GenerateHeightmap(){

		if(material != null){
			renderer.material = material;
		} else {
			renderer.material.color = Color.white;
		}
		
		Mesh mesh = GetComponent<MeshFilter>().mesh;
		int width = Mathf.Min(heightMap.width, 255);
		int height = Mathf.Min(heightMap.height, 255);
		int x = 0, y = 0;
		
		Vector3[] vertices = new Vector3[width * height];
		Vector2[] uv = new Vector2[height * width];
		Vector4[] tangents = new Vector4[height * width];
		
		Vector2 uvScale = new Vector2(1.0f / (width - 1), 1.0f / (height - 1));
		Vector3 sizeScale = new Vector3(size.x / (width -1), size.y, size.z / (height - 1));
		
		for (y = 0; y < height; y++){
			for(x = 0; x < width; x++){
				float pixelHeight = heightMap.GetPixel(x, y).grayscale;
				Vector3 vertex = new Vector3(x, pixelHeight, y);
				vertices[y*width + x] = Vector3.Scale(sizeScale, vertex);
				uv[y*width + x] = Vector2.Scale(new Vector2(x, y), uvScale);
				
				// Calculate tangent vector; used with bumpmap shaders on mesh
				Vector3 vertexL = new Vector3(x-1, heightMap.GetPixel(x-1, y).grayscale, y);
				Vector3 vertexR = new Vector3(x+1, heightMap.GetPixel(x+1, y).grayscale, y);
				Vector3 tan = Vector3.Scale(sizeScale, vertexR - vertexL).normalized;
				tangents[y * width + x] = new Vector4(tan.x, tan.y, tan.z, -1.0f);
			}
		}
		
		mesh.vertices = vertices;
		mesh.uv = uv;
		
		// build triangle indices
		int[] triangles = new int[(height -1) * (width -1) * 6];
		int index = 0;
		
		for(y = 0; y < height-1; y++){
			for(x = 0; x < width-1; x++){
				triangles[index++] = (y * width) + x;
				triangles[index++] = ((y+1)*width) + x;
				triangles[index++] = (y * width) + x + 1;
				
				triangles[index++] = ((y+1) * width) + x;
				triangles[index++] = ((y+1) * width) + x + 1;
				triangles[index++] = (y * width) + x + 1;
			}
		}
		
		mesh.triangles = triangles;
		mesh.RecalculateNormals();
		mesh.tangents = tangents;
	}

	Texture2D Calculate(){
		if(perlin == null){
			perlin = new Perlin();
		}
		fractal = new FractalNoise(h, lacunarity, octaves, perlin);
		
		for(int y = 0; y < height; y++){
			for(int x = 0; x < width; x++){
				if(gray){
					float value = fractal.HybridMultifractal(x*scale + Time.time, y*scale + Time.time, offset);
					texture.SetPixel(x, y, new Color(value, value, value, value));
				} else {
					offsetPos = Time.time;
					float valuex = fractal.HybridMultifractal(x*scale + offsetPos * 0.6f, y*scale + offsetPos*0.6f, offset);
					float valuey = fractal.HybridMultifractal(x*scale + 161.7f + offsetPos, y*scale + 161.7f + offsetPos * 0.3f, offset);
					float valuez = fractal.HybridMultifractal(x*scale + 591.1f + offsetPos, y*scale + 591.1f + offsetPos * 0.1f, offset);
					texture.SetPixel(x, y, new Color(valuex, valuey, valuez, 1));
				}
			}
		}
		
		//texture.Apply();
		return texture;
		
	}
}
