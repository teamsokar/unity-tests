﻿using UnityEngine;
using System.Collections;

public class HeightmapGenerator : MonoBehaviour {

	public Texture2D heightMap;
	public Material material;
	public Vector3 size = new Vector3(200, 30, 200);

	// Use this for initialization
	void Start () {
		GenerateHeightmap ();
	}


	void GenerateHeightmap(){
		gameObject.AddComponent<MeshFilter>();
		gameObject.AddComponent<MeshRenderer>();
		if(material != null){
			renderer.material = material;
		} else {
			renderer.material.color = Color.white;
		}

		Mesh mesh = GetComponent<MeshFilter>().mesh;
		int width = Mathf.Min(heightMap.width, 255);
		int height = Mathf.Min(heightMap.height, 255);
		int x = 0, y = 0;

		Vector3[] vertices = new Vector3[width * height];
		Vector2[] uv = new Vector2[height * width];
		Vector4[] tangents = new Vector4[height * width];

		Vector2 uvScale = new Vector2(1.0f / (width - 1), 1.0f / (height - 1));
		Vector3 sizeScale = new Vector3(size.x / (width -1), size.y, size.z / (height - 1));

		for (y = 0; y < height; y++){
			for(x = 0; x < width; x++){
				float pixelHeight = heightMap.GetPixel(x, y).grayscale;
				Vector3 vertex = new Vector3(x, pixelHeight, y);
				vertices[y*width + x] = Vector3.Scale(sizeScale, vertex);
				uv[y*width + x] = Vector2.Scale(new Vector2(x, y), uvScale);

				// Calculate tangent vector; used with bumpmap shaders on mesh
				Vector3 vertexL = new Vector3(x-1, heightMap.GetPixel(x-1, y).grayscale, y);
				Vector3 vertexR = new Vector3(x+1, heightMap.GetPixel(x+1, y).grayscale, y);
				Vector3 tan = Vector3.Scale(sizeScale, vertexR - vertexL).normalized;
				tangents[y * width + x] = new Vector4(tan.x, tan.y, tan.z, -1.0f);
			}
		}

		mesh.vertices = vertices;
		mesh.uv = uv;

		// build triangle indices
		int[] triangles = new int[(height -1) * (width -1) * 6];
		int index = 0;

		for(y = 0; y < height-1; y++){
			for(x = 0; x < width-1; x++){
				triangles[index++] = (y * width) + x;
				triangles[index++] = ((y+1)*width) + x;
				triangles[index++] = (y * width) + x + 1;

				triangles[index++] = ((y+1) * width) + x;
				triangles[index++] = ((y+1) * width) + x + 1;
				triangles[index++] = (y * width) + x + 1;
			}
		}

		mesh.triangles = triangles;
		mesh.RecalculateNormals();
		mesh.tangents = tangents;
		              

	}
	// Update is called once per frame
	void Update () {
	
	}
}
